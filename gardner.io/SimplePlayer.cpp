#include "SimplePlayer.h"
#include <QGridLayout>

SimplePlayer::SimplePlayer(QWidget *parent)
: QWidget(parent)
{
	QGridLayout *grid = new QGridLayout(this);
	grid->setSpacing(20);

	Phonon::VideoPlayer *videoPlayer = new
	Phonon::VideoPlayer(Phonon::VideoCategory, this); // This(SimplePlayer) widget is parent

	grid->addWidget(videoPlayer, 1, 0, 3, 1);
	//videoPlayer->play(Phonon::MediaSource("/Users/yogesh/Data/AFI Top 10 Movies/10.The.Wizard.Of.Oz.720p-FLAWL3SS/The.Wizard.Of.Oz.720p-FLAWL3SS.avi"));
//	videoPlayer->play(Phonon::MediaSource("/Users/yogesh/Data/2_objects.mp4"));
	videoPlayer->play(Phonon::MediaSource("http://132.206.74.124/movie.mp4"));

	setLayout(grid);
}

SimplePlayer::~SimplePlayer()
{

}