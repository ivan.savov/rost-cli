#include "SimplePlayer.h"
#include <QApplication>
#include <QDesktopWidget>
using namespace std;



/* No VideoPlayer functionality. It's just to center the window */
void center(QWidget &widget)
{
int x, y;
int screenWidth;
int screenHeight;

int WIDTH = 350;
int HEIGHT = 250;

QDesktopWidget *desktop = QApplication::desktop();

screenWidth = desktop->width();
screenHeight = desktop->height();

x = (screenWidth - WIDTH) / 2;
y = (screenHeight - HEIGHT) / 2;

widget.setGeometry(x, y, WIDTH, HEIGHT);
}

int main(int argc, char **argv)
{
QApplication a(argc, argv);
SimplePlayer playerWindow;
playerWindow.setWindowTitle("Simple Player");
playerWindow.show();
center(playerWindow);

return a.exec();
}
