#include <boost/asio.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <vector>
#include <string>
#include <thread> 
#include <iterator>

using namespace std;
using boost::asio::ip::tcp;

  vector<unsigned char> data;
void consume(){
  if(data.size()<4) return;
  int img_begin=-1, img_end=-1;  
  for(size_t i=data.size()-2; i>0; --i){
    if(img_end==-1 && data[i]==0xff && data[i+1]==0xd9){
      img_end=i;
    }
    else if(img_end!=-1 && img_begin==-1 && data[i]==0xff && data[i+1]==0xd8){
      img_begin=i;
      break;
    }
  }

  if(img_begin!=-1 && img_end !=-1){

    size_t n = img_end - img_begin+2;
    cv::Mat img = cv::imdecode(cv::Mat(1,n,CV_8UC1,(void*)(&data[img_begin]) ), CV_LOAD_IMAGE_COLOR);
    data.erase(data.begin(), data.begin()+img_end+2);    
    cv::imshow("mjpeg",img);
    cv::waitKey(1);
  }
}
int main(){
  
  //string hostname="localhost";
  string hostname="192.168.0.100";
  string port="8080";
  //string url="/?action=stream";
  string url="/video";

  boost::asio::io_service io_service; 
  tcp::resolver resolver(io_service);
  tcp::resolver::query query(hostname, port);
  tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
  tcp::socket socket(io_service);
  boost::system::error_code error;

  while(true){
    boost::asio::connect(socket, endpoint_iterator,error);
    if(error){
      cerr<<"Error connecting"<<endl;
      this_thread::sleep_for(chrono::milliseconds(1000));
      continue;
    }
    boost::asio::streambuf request;
    std::ostream rstream(&request);

    rstream << "GET "<<url<<" HTTP/1.0\r\n";
    rstream << "Host: "<<hostname<<"\r\n";
    rstream << "Accept: */*\r\n";
    rstream << "Connection: close\r\n\r\n";

    boost::asio::write(socket, request);

    boost::asio::streambuf response;
    boost::asio::read_until(socket, response, "\r\n");

    // Check that response is OK.
    std::istream response_stream(&response);
    std::string http_version;
    response_stream >> http_version;
    unsigned int status_code;
    response_stream >> status_code;
    std::string status_message;
    std::getline(response_stream, status_message);
    if (!response_stream || http_version.substr(0, 5) != "HTTP/")
      {
	std::cerr << "Invalid response\n";
	return 1;
      }
    if (status_code != 200)
      {
	std::cerr << "Response returned with status code " << status_code << "\n";
	return 1;
      }
  
    // Read the response headers, which are terminated by a blank line.
    boost::asio::read_until(socket, response, "\r\n\r\n");

    // Process the response headers.
    std::string header;
    while (std::getline(response_stream, header) && header != "\r")
      std::cerr << header << "\n";
    std::cerr << "\n";

    // Write whatever content we already have to output.
    if (response.size() > 0)
      std::cerr << &response;

    // Read until EOF, writing data to output as we go.

    //  while (boost::asio::read(socket, response, boost::asio::transfer_at_least(4096), error)){
    while (true){
      int n=boost::asio::read_until(socket, response, "\xff\xd9", error);
      if(error) break;

      data.insert(data.end(), (istreambuf_iterator<char>(&response)), 
		  istreambuf_iterator<char>());

      response.consume(response.size());
      consume();
    }
    if (error == boost::asio::error::eof){
      cerr<<"Connection closed"<<endl;
    }
    else{
      cerr<<"Unknown error"<<endl;
      throw boost::system::system_error(error);
    }
    cerr<<"Attempting to reconnect"<<endl;
  
  }







  /*  array<char,1024> buf;
  boost::system::error_code error;
  while(stream){
    size_t n = socket.read_some(boost::asio::buffer(buf),1024);
    if(error == boost::asio::error::eof){
      cerr<<"Connection closed"<<endl;
    }
    else if(error){
      throw boost::system::system_error(error);
    }
    cerr<<"read "<<n<<endl;
    data.insert(data.end(),buf.begin(),buf.begin()+n);  
    std::this_thread::sleep_for( std::chrono::milliseconds(1000));
    }*/
  return 0;
}


