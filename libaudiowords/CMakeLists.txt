cmake_minimum_required(VERSION 2.4.6)

find_package( Boost COMPONENTS program_options REQUIRED )

find_package(OpenCV REQUIRED )

#find_package(PkgConfig)
#pkg_check_modules(GST gstreamer-0.10)
#include_directories(${GST_INCLUDE_DIRS})

include_directories(src/libmfcc)
add_library(audiowords_mfcc src/libmfcc/libmfcc.c)

add_executable(words.vocab.audio src/audiowords_vocab.cpp )
target_link_libraries(words.vocab.audio ${OpenCV_LIBS} fftw3 sndfile audiowords_mfcc ${Boost_LIBRARIES})

add_executable(words.extract.audio src/audiowords.cpp)
target_link_libraries(words.extract.audio fftw3 sndfile audiowords_mfcc ${Boost_LIBRARIES})

