#!/usr/bin/env python
import csv
import os,sys
import pysrt
import nltk
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.probability import FreqDist
from nltk.stem.lancaster import LancasterStemmer

from optparse import OptionParser
reload(sys)
sys.setdefaultencoding("utf-8")

parser = OptionParser(description="Appends unique words from a subtitles file to a vocabulary file. If the vocabulary file does not exist, then creates it.")
parser.add_option("-i", "--in", dest="srt_filename",
                  help="input subtitles file", metavar="SRT_FILE")
parser.add_option("-v", "--vocab", dest="vocab_filename",
                  help="vocabulary file", metavar="VOCAB_FILE")

(opt, args) = parser.parse_args()
if(opt.srt_filename==None or opt.vocab_filename==None):
  print "ERROR: must specify subtitles filename and vocabulary filename"
  exit(1)

#read the subtitles
print("Opening subtitles file:",opt.srt_filename)
subs = pysrt.open(opt.srt_filename,  encoding='iso-8859-1')
print("Read",len(subs)," subtitle lines")


#tokenize the text, make the tokens lower case, and extract the lemm(stem)
#lemmatizer = WordNetLemmatizer()
stemmer = LancasterStemmer()
allwords=[]
for sub in subs:
	words = nltk.word_tokenize(sub.text)
	#stems = [lemmatizer.lemmatize(w.lower().strip('.,;')) for w in words]
	stems = [stemmer.stem(w.lower().strip('.,;')) for w in words]
	allwords.extend(stems)

print "Extracted",len(allwords)," words"

word_freq = FreqDist(allwords)

#remove the empty string from the vocabulary
try:
	word_freq.pop('')
except KeyError:
	None

#read the current vocabulary if provided, and append it
try:
	for key,value in csv.reader(open(opt.vocab_filename)):
		word_freq.inc(key,int(value))
except IOError:
   print 'No existing vocabulary found'

#print "Unique words: ", len(word_freq.keys())
#print word_freq.keys()
#print word_freq.values()
#print [(k,v) for (k,v) in word_freq.iteritems()]

#write the new vocabulary
count=0
try:
	out = csv.writer(open(opt.vocab_filename, "w"))
	for key, val in word_freq.items():
		out.writerow([key, val])
		if val > 1:
			count=count+1
except IOError:
	print 'Error writing to vocabulary file.'

print "Vocabulary size (# words with freq > 1) =",count