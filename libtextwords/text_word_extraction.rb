out_file = File.open(ARGV[1], 'w')
the_line = []

=begin
Here we are outputting a file with one word span per line.
The span has the start time, end time, and the words
=end

def clean_time(cleaned_start)
  ms_position = 0

  ms_position = cleaned_start[1].to_i
  cleaned_start = cleaned_start[0].each_line(":").to_a
  ms_position += cleaned_start[2].to_i * 1000
  ms_position += cleaned_start[1].to_i * 60 * 1000
  ms_position += cleaned_start[0].to_i * 60 * 60 * 1000
end

text_is_coming = false
the_text = ""

time_line_start = 0
time_line_end = 0


IO.foreach(ARGV.first) do |line|

  if text_is_coming
    the_text += ","+line.delete("\n")
  end
  if line.include? "-->"
    text_is_coming = true
    time_line = []
    time_line = line.each_line(" --> ").to_a
    time_line_start = clean_time(time_line[0].chomp(" --> ").each_line(",").to_a).to_i
    time_line_end = clean_time(time_line[1].each_line(",").to_a).to_i
  end
  if line == "\n"
    text_is_coming = false
    the_words = []

    the_text.split(%r{\W+}).each do |word|
      the_words <<  word.downcase.delete("\n")
    end

    the_words = the_words.reject {|c| c.empty? || c == "s"}

    the_text = the_words.join(",")
    the_text += "\n"


    out_file.puts(time_line_start.to_s+","+time_line_end.to_s+","+the_text)
    the_text = ""
  end    

end
out_file.close    

# the file was written with intermediary format that has to be processed again

# here it is read back in so it can be overwritten with the real right stuff
in_file = File.open(ARGV[1], "r")
data = in_file.read.split("\n")

# file is set to be written again
out_file = File.open(ARGV[1], "w")

sample = 0
sampled_data = []

i = 0
snapshots = []
# this while loop goes till the last time range's end point
while i <= data.last.split(",")[1].to_i do 
  snapshots << i
  i += ARGV[2].to_i
end


i=0
ii=0

def convert_to_integer(stuff)

  dic_file = File.open(ARGV[3], "r")

  dictionary = []
  dictionary = dic_file.read.delete("\n").split(",")

  dic_file.close

  new_stuff = []
  stuff.each do |s|
    new_stuff << dictionary.index(s)
  end
  return new_stuff
end

while i < snapshots.length do
  stuff = data[ii].split(",")

  if snapshots[i] < stuff[0].to_i
    i += 1
    time_interval = 0
  elsif ((stuff[0].to_i..stuff[1].to_i).include? snapshots[i])
    time_offset = ((stuff[1].to_f - stuff[0].to_f) / ARGV[2].to_f).to_i

    stuff2 = convert_to_integer(stuff[2..-1])
    out_file.puts((stuff[0].to_i+time_interval*ARGV[2].to_i).to_s+","+stuff2.join(",")+"\n")
    i += 1
    time_interval += 1
  elsif snapshots[i] > stuff[1].to_i
    ii += 1
    time_interval = 0
  end
  
end



out_file.close
