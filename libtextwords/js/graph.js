function Graph(media_name, options){

    var default_options = {
	callback:"",
	graph_type:"bar",
	video_object_to_graph:"video",
	graph_name:"graph",
    }
    
    media_name = typeof media_name !== 'undefined' ? media_name : "wizard_of_oz.mp4";
    options = typeof options !== 'undefined' ? options : default_options;

    var video = $("#"+options["video_object_to_graph"])[0]


    //Place the graph on the page
    $("<iframe src='topic_graph.html"+"?options="+JSON.stringify(options)+"' name='"+options["graph_name"]+"' width='100%' height='40%'></iframe>").appendTo('body'); 
    //

    var graph_topics_at_seconds = function(vid_pos){
	vid_pos = parseInt(vid_pos * 1000);
	window.frames[options["graph_name"]].graph_at_milisecs(vid_pos)
    }


    var default_callback = function(r){

	var graph = window.frames[options["graph_name"]]
	graph.set_topics_data(r);

	var video_playhead_position = null
	video.addEventListener("playing", function(){

	    clearInterval(video_playhead_position)
	    video_playhead_position = setInterval(function(){graph_topics_at_seconds(video.currentTime)},100)
	})
	video.addEventListener("seeked", function(){
	    
	    clearInterval(video_playhead_position)
	    video_playhead_position = setInterval(function(){graph_topics_at_seconds(video.currentTime)},100)
	})
	video.addEventListener("seeking", function(){
	    
	    clearInterval(video_playhead_position)
	    graph.last_found_key_index_reset();
	})
	video.addEventListener("canplay", function(){

	    clearInterval(video_playhead_position)
	    graph.last_found_key_index_reset();
	    video_playhead_position = setInterval(function(){graph_topics_at_seconds(video.currentTime)},100)
	    
	})
	video.addEventListener("pause", function(){
	    clearInterval(video_playhead_position)
	})
	video.addEventListener("ended", function(){
	    clearInterval(video_playhead_position)
	})


        video.play();	
    }

    options["callback"] = options["callback"] == "" ? default_callback : options["callback"]

    var data = ""
    
    $.get('topics/'+media_name+'.json',data,options["callback"])
    return true;

}