out_file = File.open(ARGV[1], 'w')
the_line = []

=begin
Here we are outputting a file with one word span per line.
The span has the start time, end time, and the words
=end


timestamped_topics = ""

json_formatted_text = "var timestamped_topics=["

timestamped_topics = "{"

IO.foreach(ARGV.first) do |line|
  unless line == "\n"
    bob = line.delete("\n").split(",")
    timestamp = bob.shift
    which_topics = bob.uniq
    topic_weight_string = ""
    which_topics.each do |topic|
      topic_weight = bob.count(topic) # {1234=>{18=>12, 9=>1}}
      
      topic_weight_string += "\""+topic.to_s+"\""+" : "+topic_weight.to_s+","
    end
    
    timestamped_topics += "\""+timestamp.to_s+"\""+" : {"+topic_weight_string.chop+"},"

end
end


    
  
out_file.puts(timestamped_topics.chop+"}")
  
out_file.close    
