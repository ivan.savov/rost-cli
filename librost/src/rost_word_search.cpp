#include "rost/rost.hpp"
#include "rost/program_options.hpp"
#include "rost/io.hpp"
#include <iostream>
#include <string>


#define POSE_XY
#define POSEDIM 3
typedef array<int,POSEDIM> pose_t;
typedef ROST<pose_t,neighbors<pose_t>, hash_pose_ignoretime<pose_t>, pose_equal_ignoretime<pose_t> > ROST_t;
typedef typename ROST_t::word_data_t word_data_t;
//template<typename PoseT>
/*typedef std::map<pose_t, 
                  tuple<
                    vector<int>, //words
                    vector<int>, //topics
                    vector<pose_t> //exact poses
                  >
                > word_data_t;*/

//typedef typename WordData<pose_t> word_data_t;
std::random_device rd;
std::mt19937 gen(rd());
//load words and collect them by their poses          
template<typename R>
typename R::word_data_t load_words_and_poses(const string& wordfile,  const string& topicfile, const string& posefile, int cell_time, int cell_space, bool zero_time){
//  typedef typename R::pose_t pose_t;
  constexpr size_t posedim = pose_t().size();

  csv_reader in_words(wordfile);
  csv_reader in_topics(topicfile);
  csv_reader in_poses(posefile);

  vector<int> word_line, pose_line, topic_line;

  typename R::word_data_t word_data;

  bool have_topics= (topicfile != "");
  bool have_poses= (posefile != "");

  word_line = in_words.get();
  if(have_poses) {pose_line = in_poses.get(); assert (posedim > 1);}
  if(have_topics) topic_line = in_topics.get();

  while(!word_line.empty()){
    //read a line of words
    vector<int> words(word_line.begin()+1, word_line.end());

    //build the correponding poses
    vector<pose_t> cell_poses(words.size());
    vector<pose_t> exact_poses(words.size());

    //first pose dimension is always time
    for(auto& p: cell_poses) p[0]=zero_time?0:(word_line[0]/cell_time); //cellularized pose
    for(auto& p: exact_poses) p[0]=word_line[0];

    //if have space dimension, read them
    if(posedim>1){
      assert(!pose_line.empty());
      //pose line is in format timestamp,x1,y1,x2,y2,x3,y3.....
      if(pose_line.size() != (words.size()*(posedim-1) +1) ){
        cerr<<"ERROR: poses are not synced."<<endl;        
        assert(false);
      }
      for(size_t i=0;i<cell_poses.size(); ++i){
        for(size_t d=1;d<posedim; ++d){
          exact_poses[i][d]=pose_line[i*(posedim-1)+d];
          cell_poses[i][d]=(exact_poses[i][d])/cell_space;
        }
      }     
    }

   //collect words by their similar pose
    for(size_t i=0;i<words.size(); ++i){
      get<0>(word_data[cell_poses[i]]).push_back(words[i]);
      get<2>(word_data[cell_poses[i]]).push_back(exact_poses[i]);
    }

   if(have_topics){
      if(topic_line[0]!=word_line[0] && topic_line.size() != word_line.size()){
        cerr<<"ERROR: topics and words time stamps are not synced"<<endl;
        assert(false);
      }
      vector<int> topics(topic_line.begin()+1, topic_line.end());
         //collect words by their similar pose
      for(size_t i=0;i<words.size(); ++i){
        get<1>(word_data[cell_poses[i]]).push_back(topics[i]);
      }   
    }

    word_line = in_words.get();
    if(have_poses) pose_line = in_poses.get();
    if(have_topics) topic_line = in_topics.get();
 

  }    

  return word_data;
}


template<typename R>
struct naive_word_search{
  R& rost;
  word_data_t& word_data;
  neighbors<array<int,2>> g_fun;
  int w_id;
  float grad_x; float grad_y;
  float last_grad_x; float last_grad_y;
  float lr; float momentum;

  map<array<int,2>,int> visit_count;
  naive_word_search(R& rost_, word_data_t& word_data_, int w_id_=0):
    rost(rost_),
    word_data(word_data_),
    g_fun(1),
    w_id(w_id_),
    grad_x(0), grad_y(0), last_grad_x(0), last_grad_y(0),
    lr(0.01), momentum(0.9)
    {
      cerr<<"Initializing walk: "<<endl;
    }


  float dist2(array<int,2>& p, array<int,2>& q){
    float d1(p[0]-q[0]), d2(p[1]-q[1]);
    return d1*d1 + d2*d2;
  }

  float dist3(const array<int,3>& p, const array<int,3>& q){
    float d1(p[1]-q[1]), d2(p[2]-q[2]);
    return d1*d1 + d2*d2;
  }

  //return next pose. pose dims are in order t,x,y,..
  pose_t operator()(const pose_t & current_pose3)
  {
    cerr << "@ " << current_pose3[0] << "," << current_pose3[1] << "," << current_pose3[2] << endl;
    array<int,2> current_pose2 = {0,0};
    current_pose2[0] = current_pose3[1]; current_pose2[1] = current_pose3[2];
    visit_count[current_pose2]++;

    auto current_cell = rost.get_cell(rost.cell_lookup[current_pose3]);
    vector<int>& words = get<0>(word_data[current_pose3]);
    vector<int> nZg = rost.neighborhood(*current_cell);
    vector<int> topics = rost.refine_ext(words, 10, nZg); //nZg now updated with the topics computed 
    vector<int> nZ = histogram( topics, nZg.size() ); //topic distribution of this cell

    grad_x = 0;
    grad_y = 0;
    for (auto evidence : word_data)
    {
      float d = dist3(evidence.first, current_pose3);
      vector<int> words_evidence  = std::get<0>(evidence.second);
      // ARNOLD-TODO: Hardcoded gradient size
      if (d < 20)
      {
        float dx = evidence.first[1]-current_pose3[1];
        float dy = evidence.first[2]-current_pose3[2];
        if (dx > 0)
          grad_x += (1/dx) * (words[w_id] - words_evidence[w_id]);
        if (dy > 0)
          grad_y += (1/dy) * (words[w_id] - words_evidence[w_id]);
      }
    }
    pose_t next_pose = current_pose3;
    float step_x = lr*grad_x + momentum*last_grad_x;
    last_grad_x = step_x;
    float step_y = lr*grad_y + momentum*last_grad_y;
    last_grad_y = step_y;

    next_pose[1] -= step_x;
    next_pose[2] -= step_y;

    return next_pose;

  }

};

// takes a random subset of the word data from the given pose, and adds it to the topic model
template<typename R>
void add_step_observations(R& rost, typename R::word_data_t& word_data, typename R::word_data_t& observed_word_data,typename R::pose_t & current_pose3){
    //get all the words for the current pose
    int i = current_pose3[0];
    current_pose3[0]=0;
    vector<int>& words = get<0>(word_data[current_pose3]);
    vector<pose_t>& pixel_poses = get<2>(word_data[current_pose3]);

    //get a random subset of the words
    uniform_int_distribution<> dist(0, words.size()-1);
    vector<int> step_words;
    vector<pose_t> step_pixel_poses;
    // ARNOLD-TODO: add_step_observatios is hardcoded to take 1/4 of the words for the given pose, parameterize or otherwise fix
    for(size_t wi=0;wi<words.size()/4; ++wi){
      int rand_wi = dist(gen);
      step_words.push_back(words[rand_wi]);
      step_pixel_poses.push_back(pixel_poses[rand_wi]);
      step_pixel_poses.back()[0]=i;
    }

    //add the random subset of the words to the topic model
    current_pose3[0]=i;
    rost.add_observation(current_pose3,step_words);

    vector<int>& cell_words=get<0>(observed_word_data[current_pose3]);
    vector<pose_t>& cell_poses=get<2>(observed_word_data[current_pose3]);
    cell_words.insert(cell_words.end(), step_words.begin(), step_words.end());
    cell_poses.insert(cell_poses.end(), step_pixel_poses.begin(), step_pixel_poses.end());

    //cerr<<"adding: "<<current_pose3[0]<<","<<current_pose3[1]<<","<<current_pose3[2]<<endl;

}

int main(int argc, char*argv[]){

  po::options_description options("Topic modeling of data with 1 dimensional structure.");
  load_rost_base_options(options).add_options()
    ("topicmodel.update", po::value<bool>()->default_value(true), "Update global topic model with each iteration")
    ("out.wordppx", po::value<string>()->default_value("topics.wordppx.csv"), "P(w|model) for each word") 
    ("out.path", po::value<string>()->default_value("path.csv"), "path taken by the explorer") 
    ("in.words.position",   po::value<string>()->default_value(""),"Word position csv file.")
    ("out.position",   po::value<string>()->default_value("topics.position.csv"),"Position data for topics.")
    ("step.time",   po::value<int>()->default_value(100),"refine time between two steps")
    ("steps",   po::value<int>()->default_value(100),"Number of steps to take")
  ;
  
  auto args = read_command_line(argc, argv, options);

  if(!args.count("vocabsize")){
    cerr<<"ERROR: Must specify vocabulary size."<<endl;
    exit(0);
  }


  pose_t G, cell_size;
  G[0] = args["g.time"].as<int>();
  cell_size[0] = args["cell.time"].as<int>();
  for(int i = 1;i < G.size(); ++i)
  {
    G[i] = args["g.space"].as<int>();
    cell_size[i] = args["cell.space"].as<int>();
  }

  ROST_t rost(args["vocabsize"].as<int>(),
              args["ntopics"].as<int>(),
              args["alpha"].as<double>(),
              args["beta"].as<double>(), 
              G);
  
  Logger logger(args["logfile"].as<string>());

  logger
    << "words=" << args["in.words"].as<string>() << "\n"
    << "alpha=" << args["alpha"].as<double>() << "\n"
    << "beta=" << args["beta"].as<double>() << "\n"
    << "g.space=" << args["g.space"].as<int>() << "\n"
    << "g.time=" << args["g.time"].as<int>() << "\n"
    << "K=" << args["ntopics"].as<int>() << "\n"
    << "V=" << args["vocabsize"].as<int>() << "\n"
    << "cell.space=" << args["cell.space"].as<int>() << "\n"
    << "step.time=" << args["step.time"].as<int>() << "\n";


  rost.update_global_model = args["topicmodel.update"].as<bool>();
  logger << "topicmodel.update=" << rost.update_global_model << "\n";


  cerr << "Loading data..";
  auto word_data =  load_words_and_poses<ROST_t>( args["in.words"].as<string>(), 
                                args["in.topics"].as<string>(), 
                                args["in.words.position"].as<string>(),
                                args["cell.time"].as<int>(), 
                                args["cell.space"].as<int>(),
                                true); //ignore timestamp in pose
  cerr << "Done" << endl;

  word_data_t observed_word_data;

  //start the engines
  atomic<bool> stop;
  stop.store(false);
  double tau = args["tau"].as<double>();
  int n_threads = args["threads"].as<int>();  
  //auto workers =  parallel_refine_online_exp(&rost, tau , n_threads, &stop);

  // ARNOLD-TODO : Why are these hardcoded?
  auto workers = parallel_refine_online2(&rost, 2.0 , 0.5, 1, n_threads, &stop);

  int n_steps = args["steps"].as<int>();
  array<int,2> current_pose2 = {0,0};
  neighbors<array<int,2>> g_fun(1);

  //choose a random start
  vector<pose_t> poses;
  for(auto & kv: word_data)
    poses.push_back(kv.first);
  
  int start_i = uniform_int_distribution<>()(gen) % poses.size();
  pose_t current_pose3 = poses[start_i];
  current_pose3[0] = 0;


  naive_word_search<ROST_t> next_step(rost, word_data);
  vector<pose_t> path;

  // Main Loop
  for(int i = 0; i < n_steps; ++i){
    
    add_step_observations(rost, word_data, observed_word_data, current_pose3);

    path.push_back(current_pose3);

    //sleep a little to let the topics evovle
    this_thread::sleep_for(chrono::milliseconds(args["step.time"].as<int>()));
    current_pose3 = next_step(current_pose3);

  }

  stop.store(true);
  for(auto t:workers){
    t->join();
  }

  cerr << "Writing position file:" << args["out.position"].as<string>() << "\n";
  write_poses(rost, args["out.position"].as<string>(), observed_word_data);

  cerr << "Writing ML topics to:" << args["out.topics.ml"].as<string>() << "\n";
  write_topics(rost, args["out.topics.ml"].as<string>(), observed_word_data, true);

  cerr << "Writing topics to:" << args["out.topics"].as<string>() << "\n";
  write_topics(rost, args["out.topics"].as<string>(), observed_word_data, false);

  cerr << "Writing path to:" << args["out.path"].as<string>() << "\n";
  write_path(args["out.path"].as<string>(), path, cell_size);

  if ( args["out.topicmodel"].as<string>() != "" )
    save_topic_model(rost,args["out.topicmodel"].as<string>());

  return 0;
}
