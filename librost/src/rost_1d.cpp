#include "rost/rost.hpp"
#include "rost/program_options.hpp"
#include "io.hpp"
#include "rost/csv_reader.hpp"
#include <iostream>
#include <string>


int main(int argc, char*argv[]){

  po::options_description options("Topic modeling of data with 1 dimensional structure.");
  load_rost_base_options(options).add_options()
    ("neighborhood.size,g", po::value<int>()->default_value(1), "Depth of the neighborhood")
    ("in.topicmask",po::value<string>(),"Mask file for topics. Format is k lines of 0 or 1, where 0=> don't use the topic")
    ("add.to.topicmodel", po::value<bool>()->default_value(true), "Add the given topic labels to topic model. Only applicable when a topic model and topics are provided")
    ("out.intermediate.topics", po::value<bool>()->default_value(false), "output intermediate topics") 
    ;
  //("iter.write.topics", po::value<bool>()->default_value(false), "write intermediate topics")
  
  auto args = read_command_line(argc, argv, options);

  if(!args.count("vocabsize")){
    cerr<<"ERROR: Must specify vocabulary size."<<endl;
    exit(0);
  }
  
  ROST<int,neighbors<int>, hash<int> > rost(args["vocabsize"].as<int>(),
                 args["ntopics"].as<int>(),
                 args["alpha"].as<double>(),
                 args["beta"].as<double>(),
                 neighbors<int>(args["neighborhood.size"].as<int>()),
                 hash<int>());
  
  Logger logger(args["logfile"].as<string>());

  logger
    <<"words="<< args["in.words"].as<string>()<<"\n"
    <<"alpha="<< args["alpha"].as<double>()<<"\n"
    <<"beta="<< args["beta"].as<double>()<<"\n"
    <<"g="<< args["neighborhood.size"].as<int>()<<"\n"
    <<"K="<<  args["ntopics"].as<int>()<<"\n"
    <<"V="<< args["vocabsize"].as<int>()<<"\n"
    <<"iterations="<< args["iter"].as<int>()<<"\n";


  vector<int> topicmask;
  if(args.count("in.topicmask")){
    ifstream maskin(args["in.topicmask"].as<string>());
    int m;
    maskin >>m;
    cerr<<"Reading mask: ";
    while(maskin){
      cerr<<m<<" ";
      if(m!=0 && m!=1){
        cerr<<"Error reading mask file. mask values must be 0 or 1.\n";
      }
      topicmask.push_back(m);
      maskin >> m;
    }
    assert(topicmask.size()==args["ntopics"].as<int>());
    rost.mask_Z = topicmask;
  }

  //load the KxV topic model if specified
  bool add_to_topic_model=true;
  if(args["in.topicmodel"].as<string>()!=""){
    //read the topic model as a CSV file
    logger<<"Loading topic model: "<<args["in.topicmodel"].as<string>()<<"\n";
    csv_reader topic_model_reader(args["in.topicmodel"].as<string>(),','); 
    vector<int> topic_model;
    vector<int> topic_weights;
    vector<int> topic = topic_model_reader.get();
    while(!topic.empty()){
      topic_model.insert(topic_model.end(), topic.begin(), topic.end());
      topic_weights.push_back(std::accumulate(topic.begin(), topic.end(), 0));
      topic = topic_model_reader.get();
    }
    rost.set_topic_model(topic_model, topic_weights);
    add_to_topic_model=args["add.to.topicmodel"].as<bool>();
  }
  
  csv_reader in(args["in.words"].as<string>(), args["in.words.delim"].as<string>()[0]); 


  ofstream out_ppx_iter("perplexity.iter.csv");
  //out_ppx<<"#iter,ppx_t1,ppx_t2,....ppx_tT,ppx_global"<<endl;

  vector<int> timestamps;
  if(!args.count("online")){
    logger<<"Processing words in batch mode.\n"; 
    //read the words
    int t=0;

    //load words with the given initial topics
    if(args["in.topics"].as<string>()!=""){
      csv_reader intopics(args["in.topics"].as<string>(), args["in.words.delim"].as<string>()[0]);
      auto topics = intopics.get();
      auto words = in.get();

      logger<<"Loading word and initial topic labels..\n";
      while(!words.empty()){

        assert(!topics.empty());
        assert(topics.size() == words.size());
        assert(words[0]==topics[0]);

        timestamps.push_back(words[0]);
        rost.add_observation(t++,words.begin()+1, words.end(), add_to_topic_model, topics.begin()+1, topics.end());
        words = in.get();
        topics = intopics.get();
      }      
    }
    else{//just load the words 

      auto words = in.get();
      logger<<"Loading word data..\n";
      while(!words.empty()){
        timestamps.push_back(words[0]);
        rost.add_observation(t++,words.begin()+1, words.end());
        words = in.get();
      }

    }

    logger<<"Read "<<rost.cells.size()<<" cells\n";

    std::chrono::time_point<std::chrono::system_clock> start_time, end_time;
    int elapsed_time=0;
    for(int i=0; i<args["iter"].as<int>(); ++i){
      cerr<<"iter: "<<i;
      start_time = chrono::system_clock::now();
      parallel_refine(&rost, args["threads"].as<int>());
      end_time = chrono::system_clock::now();
      elapsed_time = std::chrono::duration_cast<std::chrono::milliseconds>
                             (end_time-start_time).count();
      cerr<<", iter_time:"<<elapsed_time/1000.0<<"sec.          \r";

      //write perplexity
      if((args["ppx.rate"].as<int>() >0) && 
	 (i%args["ppx.rate"].as<int>()==0 || i==(args["iter"].as<int>() -1))) {
        cerr<<"\nComputing perplexity..."<<endl;

        ofstream out_ppx(string("perplexity_")+to_string(i)+".csv");
        for(auto& pose : rost.cell_pose){
          out_ppx <<pose<<","<<rost.perplexity(pose,true)<<endl;
        }
        out_ppx.close();

        double global_ppx = rost.perplexity(false); //don't recompute cell ppx
        logger<<"iteration="<<i<<" perplexity="<<global_ppx<<"\n";
        out_ppx_iter<<global_ppx<<endl;

        if(args["out.intermediate.topics"].as<bool>()){
          cerr<<"Writing intermediate topics results"<<endl;
          ofstream out_topics(string("topics_")+to_string(i)+".csv");

          for(int d = 0; d< rost.C; ++d){
            vector<int> topics = rost.cells[d]->Z;//rost.get_topics_for_pose(rost.cell_pose[d]);
            out(out_topics,timestamps[d],topics);
          }
          out_topics.close();
        }

      }
    }
    cerr<<endl;
    logger<<"Writing topics to:"<<args["out.topics"].as<string>()<<"\n";

    ofstream out_topics(args["out.topics"].as<string>());
    for(int d = 0; d< rost.C; ++d){
      vector<int> topics = rost.cells[d]->Z;//rost.get_topics_for_pose(rost.cell_pose[d]);
      out(out_topics,timestamps[d],topics);
    }

    if(args["out.topics.ml"].as<string>()!="" && args["out.topics.ml"].as<string>()!="0"){
      logger<<"Computing and writing maximum likelihood topics to: "<<args["out.topics.ml"].as<string>()<<"\n";
      ofstream out_topics_ml(args["out.topics.ml"].as<string>());
      ofstream out_ppx(args["ppx.out"].as<string>());

      for(int d = 0; d< rost.C; ++d){
        vector<int> topics; double ppx;
        tie(topics,ppx) = rost.get_ml_topics_and_ppx_for_pose(rost.cell_pose[d]);
        out(out_topics_ml,timestamps[d],topics);
        out_ppx <<rost.cell_pose[d]<<","<<rost.perplexity(rost.cell_pose[d],false)<<endl;
      }
    }
  
  }
  ////////////////////////////////////////////////////////////////////////////////////////////////////
  //online processing
  else{
    cerr<<"Processing words online."<<endl;
    atomic<bool> stop;
    stop.store(false);
    auto workers =  parallel_refine_online(&rost, args["tau"].as<double>(), args["threads"].as<int>(), &stop);
    ofstream out_topics(args["out.topics"].as<string>());


    auto words = in.get();

    int t=0;

    //cerr<<"adasdasd"<<endl;
    bool first=true;
    while(!words.empty()){
      timestamps.push_back(words[0]);
      this_thread::sleep_for(chrono::milliseconds(args["online.mint"].as<int>()));
      //output the topics from the previous time step.
      if(!first){
        vector<int> topics = rost.get_ml_topics_for_pose(t-1);
        out(out_topics,timestamps[t-1],topics);
      }
      else first=false;

      rost.add_observation(t++,words.begin()+1, words.end());
      words = in.get();            
    }
    vector<int> topics = rost.get_ml_topics_for_pose(t-1);
    out(out_topics,timestamps[t-1],topics);
   
    cerr<<"Reached EOF. Terminating."<<endl;
    stop.store(true);
    for(auto t:workers){
      t->join();
    }
  }


  if(args["out.topicmodel"].as<string>()!=""){
    cerr<<"Writing topicmodel to: "<<args["out.topicmodel"].as<string>()<<endl;
    ofstream out_topic_model(args["out.topicmodel"].as<string>());
    vector<vector<int> > topic_model = rost.get_topic_model();
    for(auto & topic: topic_model){
      out_topic_model<<topic[0];
      for(size_t i = 1; i< topic.size(); ++i){
        out_topic_model<<","<<topic[i];
      }
      out_topic_model<<endl;
    }
  }

  return 0;
}
