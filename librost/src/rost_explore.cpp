#include "rost/rost.hpp"
#include "rost/program_options.hpp"
#include "rost/io.hpp"
#include <iostream>
#include <string>


#define POSE_XY
#define POSEDIM 3
typedef array<int,POSEDIM> pose_t;
typedef ROST<pose_t,neighbors<pose_t>, hash_pose_ignoretime<pose_t>, pose_equal_ignoretime<pose_t> > ROST_t;
typedef typename ROST_t::word_data_t word_data_t;
//template<typename PoseT>
/*typedef std::map<pose_t, 
                  tuple<
                    vector<int>, //words
                    vector<int>, //topics
                    vector<pose_t> //exact poses
                  >
                > word_data_t;*/

//typedef typename WordData<pose_t> word_data_t;
std::random_device rd;
std::mt19937 gen(rd());
//load words and collect them by their poses          
template<typename R>
typename R::word_data_t load_words_and_poses(const string& wordfile,  const string& topicfile, const string& posefile, int cell_time, int cell_space, bool zero_time){
//  typedef typename R::pose_t pose_t;
  constexpr size_t posedim = pose_t().size();

  csv_reader in_words(wordfile);
  csv_reader in_topics(topicfile);
  csv_reader in_poses(posefile);

  vector<int> word_line, pose_line, topic_line;

  typename R::word_data_t word_data;

  bool have_topics= (topicfile != "");
  bool have_poses= (posefile != "");

  word_line = in_words.get();
  if(have_poses) {pose_line = in_poses.get(); assert (posedim > 1);}
  if(have_topics) topic_line = in_topics.get();

  while(!word_line.empty()){
    //read a line of words
    vector<int> words(word_line.begin()+1, word_line.end());

    //build the correponding poses
    vector<pose_t> cell_poses(words.size());
    vector<pose_t> exact_poses(words.size());

    //first pose dimension is always time
    for(auto& p: cell_poses) p[0]=zero_time?0:(word_line[0]/cell_time); //cellularized pose
    for(auto& p: exact_poses) p[0]=word_line[0];

    //if have space dimension, read them
    if(posedim>1){
      assert(!pose_line.empty());
      //pose line is in format timestamp,x1,y1,x2,y2,x3,y3.....
      if(pose_line.size() != (words.size()*(posedim-1) +1) ){
        cerr<<"ERROR: poses are not synced."<<endl;        
        assert(false);
      }
      for(size_t i=0;i<cell_poses.size(); ++i){
        for(size_t d=1;d<posedim; ++d){
          exact_poses[i][d]=pose_line[i*(posedim-1)+d];
          cell_poses[i][d]=(exact_poses[i][d])/cell_space;
        }
      }     
    }

   //collect words by their similar pose
    for(size_t i=0;i<words.size(); ++i){
      get<0>(word_data[cell_poses[i]]).push_back(words[i]);
      get<2>(word_data[cell_poses[i]]).push_back(exact_poses[i]);
    }

   if(have_topics){
      if(topic_line[0]!=word_line[0] && topic_line.size() != word_line.size()){
        cerr<<"ERROR: topics and words time stamps are not synced"<<endl;
        assert(false);
      }
      vector<int> topics(topic_line.begin()+1, topic_line.end());
         //collect words by their similar pose
      for(size_t i=0;i<words.size(); ++i){
        get<1>(word_data[cell_poses[i]]).push_back(topics[i]);
      }   
    }

    word_line = in_words.get();
    if(have_poses) pose_line = in_poses.get();
    if(have_topics) topic_line = in_topics.get();
 

  }    

  return word_data;
}



//Explorers
struct random_walk{
  word_data_t& word_data;
  neighbors<array<int,2>> g_fun;
  //std::mt19937 gen;

  random_walk(word_data_t& word_data_):
    word_data(word_data_),
    g_fun(1)
    {}
  //return next pose. pose dims are in order t,x,y,..
  pose_t operator()(const pose_t & current_pose3){
    pose_t next_pose;
    array<int,2> current_pose2={0,0};
    vector<pose_t> possible_next_steps;
    current_pose2[0]=current_pose3[1]; current_pose2[1]=current_pose3[2];
    for(array<int,2>& g: g_fun(current_pose2)){
      pose_t g_pose={0,g[0],g[1]};
      auto g_it = word_data.find(g_pose);
      if(g_it != word_data.end()){
        possible_next_steps.push_back(g_pose);
      }
    }
    uniform_int_distribution<> next_step_dist(0, possible_next_steps.size()-1);
    next_pose = possible_next_steps[next_step_dist(gen)];
    next_pose[0]=current_pose3[0]+1;    
    return next_pose;
  }
};


template<typename R>
struct ppx_walk{
  R& rost;
  word_data_t& word_data;
  neighbors<array<int,2>> g_fun;
  //std::mt19937 gen;
  bool greedy,repulsive_path;
  int ppxtype;
  bool normalize_weights;
  map<array<int,2>,int> visit_count;
  int influence;
  ppx_walk(R& rost_, word_data_t& word_data_, bool greedy_, int ppxtype_, bool normalize_, bool repulsive_path_):
    rost(rost_),
    word_data(word_data_),
    g_fun(1),
    greedy(greedy_),
    ppxtype(ppxtype_),
    normalize_weights(normalize_),
    influence(5),
    repulsive_path(repulsive_path_)
    {
      cerr<<"Initializing walk: "<<endl
          <<"\ttype:     "<<ppxtype<<endl
          <<"\tgreedy:   "<<greedy<<endl
          <<"\tnormalize:"<<normalize_weights<<endl
          <<"\trepulsive_path:"<<repulsive_path<<endl
          <<"\tinflucence: "<<influence<<endl;
    }


  float dist2(array<int,2>& p, array<int,2>& q){
    float d1(p[0]-q[0]), d2(p[1]-q[1]);
    return d1*d1 + d2*d2;
  }
  float potential(array<int,2>& pose2){
    float u=0;
    for(int x=pose2[0]-influence; x<=pose2[0]+influence; ++x){
      for(int y=pose2[1]-influence; y<=pose2[1]+influence; ++y){        
        array<int,2> cell={x,y};
        if(cell == pose2)
          continue;
        pose_t pose3={0,pose2[0],pose2[1]};
        float charge;
        if(word_data.find(pose3) == word_data.end()){
          //not in the world
          charge=10;
        }
        else{
          charge=visit_count[cell];
        }

        float d2=dist2(cell,pose2);
         u+= (charge / d2);
      }
    }
    return u;
  }

  //return next pose. pose dims are in order t,x,y,..
  pose_t operator()(const pose_t & current_pose3){
    cerr<<"@ "<<current_pose3[0]<<","<<current_pose3[1]<<","<<current_pose3[2]<<endl;
    pose_t next_pose;
    array<int,2> current_pose2={0,0};
    vector<pose_t> possible_next_steps;
    vector<double> step_ppx;
    current_pose2[0]=current_pose3[1]; current_pose2[1]=current_pose3[2];
    visit_count[current_pose2]++;

    auto current_cell = rost.get_cell(rost.cell_lookup[current_pose3]);
    vector<int> nZg = rost.neighborhood(*current_cell);

    cerr<<"Neighbors: ";
    for(array<int,2>& g: g_fun(current_pose2)){
      if(g == current_pose2) continue;
      pose_t g_pose={0,g[0],g[1]};
      auto g_it = word_data.find(g_pose);
      if(g_it != word_data.end()){
        possible_next_steps.push_back(g_pose);
        double weight;
        vector<int>& words = get<0>(g_it->second);
	vector<int> nZgg = nZg; //nZgg = nZg + nZ
        vector<int> topics = rost.refine_ext(words,10,nZgg); //nZgg now updated with the topics computed 
        vector<int> nZ = histogram(topics,nZg.size()); //topic distribution of this cell

        switch(ppxtype){
	  case 0: weight=1.0; break;
          case 1: weight=rost.cell_perplexity_word(words,nZg); break; //perplexity of observing these words given the local topic distribution, and current topic model
  	  case 2: weight=rost.cell_perplexity_word(words,rost.weight_Z); break; //perplexity of observing these words, given the global topic distribution, and current topic model
	  case 3: weight=rost.cell_perplexity_topic(nZ); break;//perplexity of observing topics, given the global topic distribution
          default: assert(false);
        }
        float g_potential=1.0;
        if(repulsive_path){
          g_potential = potential(g);
        }
        cerr<<"("<<to_string<2>(g)<<"):"<<weight<<"/"<<g_potential<<"="<<weight/g_potential<<"  ";
        weight/=g_potential;
        step_ppx.push_back(weight);        
      }
    }
    if(normalize_weights){
      double minw = *min_element(step_ppx.begin(), step_ppx.end());
      double maxw = *max_element(step_ppx.begin(), step_ppx.end());
      if(maxw == minw) minw=0;
      for(auto& w: step_ppx){ 
        w -= minw; //cerr<<w<<"  ";
      }
      //cerr<<endl;
    }

    cerr<<endl;
    if(greedy){
      //cerr<<"greedy select"<<endl;
      size_t next=max_element(step_ppx.begin(),step_ppx.end())-step_ppx.begin();
      next_pose = possible_next_steps[next];
    }
    else{
      //cerr<<"random select"<<endl;
      discrete_distribution<> next_step_dist(step_ppx.begin(),step_ppx.end());
      next_pose = possible_next_steps[next_step_dist(gen)];
    }
    next_pose[0]=current_pose3[0]+1;
    return next_pose;
  }

};

// takes a random subset of the word data from the given pose, and adds it to the topic model
template<typename R>
void add_step_observations(R& rost, typename R::word_data_t& word_data, typename R::word_data_t& observed_word_data,typename R::pose_t & current_pose3){
    //get all the words for the current pose
    int i = current_pose3[0];
    current_pose3[0]=0;
    vector<int>& words = get<0>(word_data[current_pose3]);
    vector<pose_t>& pixel_poses = get<2>(word_data[current_pose3]);

    //get a random subset of the words
    uniform_int_distribution<> dist(0, words.size()-1);
    vector<int> step_words;
    vector<pose_t> step_pixel_poses;
    for(size_t wi=0;wi<words.size()/4; ++wi){
      int rand_wi = dist(gen);
      step_words.push_back(words[rand_wi]);
      step_pixel_poses.push_back(pixel_poses[rand_wi]);
      step_pixel_poses.back()[0]=i;
    }

    //add the random subset of the words to the topic model
    current_pose3[0]=i;
    rost.add_observation(current_pose3,step_words);

    vector<int>& cell_words=get<0>(observed_word_data[current_pose3]);
    vector<pose_t>& cell_poses=get<2>(observed_word_data[current_pose3]);
    cell_words.insert(cell_words.end(), step_words.begin(), step_words.end());
    cell_poses.insert(cell_poses.end(), step_pixel_poses.begin(), step_pixel_poses.end());

    //cerr<<"adding: "<<current_pose3[0]<<","<<current_pose3[1]<<","<<current_pose3[2]<<endl;

}

template<typename R>
void add_neighboring_observations(R& rost, typename R::word_data_t& word_data, typename R::word_data_t& observed_word_data,typename R::pose_t & current_pose3){
  neighbors<array<int,2>> g_fun(1);
  array<int,2> current_pose2={current_pose3[1], current_pose3[2]};

  for(array<int,2>& g: g_fun(current_pose2)){
    if(g == current_pose2) continue;
    pose_t g_pose={0,g[0],g[1]};
    auto g_it = word_data.find(g_pose);
    if(g_it != word_data.end()){
      g_pose[0]=current_pose3[0];
      add_step_observations(rost,word_data,observed_word_data,g_pose);
    }
  }

}

enum{W_UNIFORM=0, W_WPPX_LOCAL, W_WPPX_GLOBAL, W_ZPPX_GLOBAL};
int main(int argc, char*argv[]){

  po::options_description options("Topic modeling of data with 1 dimensional structure.");
  load_rost_base_options(options).add_options()
    //("in.topicmask",po::value<string>(),"Mask file for topics. Format is k lines of 0 or 1, where 0=> don't use the topic")
    //("add.to.topicmodel", po::value<bool>()->default_value(true), "Add the given initial topic labels to topic model. Only applicable when a topic model and topics are provided")
    //("out.intermediate.topics", po::value<bool>()->default_value(false), "output intermediate topics") 
    ("topicmodel.update", po::value<bool>()->default_value(true), "Update global topic model with each iteration")
    ("out.wordppx", po::value<string>()->default_value("topics.wordppx.csv"), "P(w|model) for each word") 
    ("out.wordtopicppx", po::value<string>()->default_value("topics.wordtopicppx.csv"), "P(z|model) for each word") 
    ("out.globalppx", po::value<string>()->default_value("topics.globalppx.csv"), "final perplexity of the entire dataset") 
    ("out.path", po::value<string>()->default_value("path.csv"), "path taken by the explorer") 
    ("in.words.position",   po::value<string>()->default_value(""),"Word position csv file.")
    ("out.position",   po::value<string>()->default_value("topics.position.csv"),"Position data for topics.")
    ("step.time",   po::value<int>()->default_value(100),"refine time between two steps")
    ("steps",   po::value<int>()->default_value(100),"Number of steps to take")
    ("uniform-weight","Use random walk exploration")
    ("wppx-local-weight","word perplexity with local context exploration")
    ("wppx-global-weight","word perplexity with global context  exploration")
    ("zppx-global-weight","topic perplexity with global context exploration")
    //    ("zppx-local-weight","topic perplexity with local context exploration")
    //("zppx-weight-2","random topic ppx exploration")
    ("repulsive-path","path repulsion")
    ("greedy-step","gredily pick the next step instead of random.")
    //("in.topics.position", po::value<string>()->default_value(""),"Initial topic label positions")    

    ;
  //("iter.write.topics", po::value<bool>()->default_value(false), "write intermediate topics")

  auto args = read_command_line(argc, argv, options);

  if(!args.count("vocabsize")){
    cerr<<"ERROR: Must specify vocabulary size."<<endl;
    exit(0);
  }
  int weight_type;
  if(args.count("uniform-weight")){
    weight_type=W_UNIFORM;
  }
  else if(args.count("wppx-local-weight")){
    weight_type=W_WPPX_LOCAL;
  }
  else if(args.count("wppx-global-weight")){
    weight_type=W_WPPX_GLOBAL;
  }
  else if(args.count("zppx-global-weight")){
    weight_type=W_ZPPX_GLOBAL;
  }
  //else if(args.count("zppx-weight-2")){
//    weight_type=W_ZPPX2;
//  }
  else{
    cerr<<"ERROR: Must specify step weight type"<<endl;
    exit(0);
  }

  pose_t G, cell_size;
  G[0]=args["g.time"].as<int>();
  cell_size[0]=args["cell.time"].as<int>();
  for(int i=1;i<G.size(); ++i){
    G[i]=args["g.space"].as<int>();
    cell_size[i]=args["cell.space"].as<int>();
  }

  ROST_t rost(args["vocabsize"].as<int>(),
              args["ntopics"].as<int>(),
              args["alpha"].as<double>(),
              args["beta"].as<double>(), 
              G);
  
  Logger logger(args["logfile"].as<string>());

  logger
    <<"words="<< args["in.words"].as<string>()<<"\n"
    <<"alpha="<< args["alpha"].as<double>()<<"\n"
    <<"beta="<< args["beta"].as<double>()<<"\n"
    <<"g.space="<< args["g.space"].as<int>()<<"\n"
    <<"g.time="<< args["g.time"].as<int>()<<"\n"
    <<"K="<<  args["ntopics"].as<int>()<<"\n"
    <<"V="<< args["vocabsize"].as<int>()<<"\n"
    <<"cell.space="<< args["cell.space"].as<int>()<<"\n"
    <<"step.time="<< args["step.time"].as<int>()<<"\n";


  rost.update_global_model=args["topicmodel.update"].as<bool>();
  logger<<"topicmodel.update="<<rost.update_global_model<<"\n";
  
  if(args.count("in.topicmask"))
    load_topic_mask(rost,args["in.topicmask"].as<string>());
  

  //load the KxV topic model if specified
  bool add_to_topic_model=true;
  if(args["in.topicmodel"].as<string>()!=""){
    load_topic_model(rost,args["in.topicmodel"].as<string>());
    add_to_topic_model=args["add.to.topicmodel"].as<bool>();
  }


  //ofstream out_ppx_iter("perplexity.iter.csv");
  //out_ppx<<"#iter,ppx_t1,ppx_t2,....ppx_tT,ppx_global"<<endl;

  ////////////////////////////////////////////  
  ////////////////////////////////////////////

  cerr<<"Loading data..";
  auto word_data =  load_words_and_poses<ROST_t>( args["in.words"].as<string>(), 
                                args["in.topics"].as<string>(), 
                                args["in.words.position"].as<string>(),
                                args["cell.time"].as<int>(), 
                                args["cell.space"].as<int>(),
                                true); //ignore timestamp in pose
  cerr<<"Done"<<endl;

  word_data_t observed_word_data;

  //start the engines
  atomic<bool> stop;
  stop.store(false);
  double tau=args["tau"].as<double>();
  int n_threads=args["threads"].as<int>();  
  //auto workers =  parallel_refine_online_exp(&rost, tau , n_threads, &stop);
  auto workers =  parallel_refine_online2(&rost, 2.0 , 0.5, 1, n_threads, &stop);


  int n_steps=args["steps"].as<int>();
  array<int,2> current_pose2={0,0};
  neighbors<array<int,2>> g_fun(1);

  //choose a random start
  vector<pose_t> poses;
  for(auto & kv: word_data){
    poses.push_back(kv.first);
  }
  int start_i = uniform_int_distribution<>()(gen) % poses.size();
  pose_t current_pose3=poses[start_i];
  current_pose3[0]=0;


  //std::random_device rd;
  //std::mt19937 gen(rd());
//  random_walk random_step(word_data);
  bool repulsive_path = args.count("repulsive-path");
  bool greedy = args.count("greedy");
  bool normalize_weights = true; //(weight_type != W_UNIFORM);

  ppx_walk<ROST_t> next_step(rost,word_data,greedy,weight_type,normalize_weights,repulsive_path);

  vector<pose_t> path;

  for(int i=0;i<n_steps; ++i){
    
    add_step_observations(rost,word_data,observed_word_data,current_pose3);
    //add_neighboring_observations(rost,word_data,observed_word_data,current_pose3);
    path.push_back(current_pose3);

    //cerr<<current_pose3[0]<<","<<current_pose3[1]<<","<<current_pose3[2]<<endl;

    //sleep a little to let the topics evovle
    this_thread::sleep_for(chrono::milliseconds(args["step.time"].as<int>()));
    current_pose3 = next_step(current_pose3);

  }

  stop.store(true);
  for(auto t:workers){
    t->join();
  }

//  cerr<<"Writing perplexity:"<<args["ppx.out"].as<string>()<<"\n";
  cerr<<"Writing position file:"<<args["out.position"].as<string>()<<"\n";
  write_poses(rost,args["out.position"].as<string>(),observed_word_data);

  cerr<<"Computing globalppx"<<endl;
  double final_ppx = rost.perplexity(true);
  cerr<<"Writing global perplexity:"<<args["out.globalppx"].as<string>()<<"\n";

  ofstream final_ppx_out(args["out.globalppx"].as<string>());
  final_ppx_out<<final_ppx<<endl;

  cerr<<"Writing word perplexity:"<<args["out.wordppx"].as<string>()<<"\n";
  write_word_perplexity(rost,args["out.wordppx"].as<string>(), observed_word_data,0);

  cerr<<"Writing word perplexity:"<<args["out.wordtopicppx"].as<string>()<<"\n";
  write_word_perplexity(rost,args["out.wordtopicppx"].as<string>(), observed_word_data,1);

  cerr<<"Writing ML topics to:"<<args["out.topics.ml"].as<string>()<<"\n";
  write_topics(rost,args["out.topics.ml"].as<string>(), observed_word_data, true);

  cerr<<"Writing topics to:"<<args["out.topics"].as<string>()<<"\n";
  write_topics(rost,args["out.topics"].as<string>(), observed_word_data, false);

  cerr<<"Writing path to:"<<args["out.path"].as<string>()<<"\n";
  write_path(args["out.path"].as<string>(), path, cell_size);

  if(args["out.topicmodel"].as<string>()!=""){
    save_topic_model(rost,args["out.topicmodel"].as<string>());
  }

  return 0;
}
