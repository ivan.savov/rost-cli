//#include "summarizer.hpp"
#include "rost/markov.hpp"
#include "rost/csv_reader.hpp"
#include "rost/distancefun.hpp"

//#include "random.hpp"
#include <map>
#include <iostream>
#include <vector>
#include <limits>
#include <future>
using namespace std;
#define _GTHREAD_USE_MUTEX_INIT_FUNC

#include <boost/program_options.hpp>

namespace po = boost::program_options;

po::options_description& load_rost_base_options( po::options_description & desc){
  desc.add_options()
    ("help", "help")    
    ("in.words,i",   po::value<string>(),"timestamped word/topic csv data ")
    ("in.format,f",   po::value<string>()->default_value("words"),"format: [words|hist] ")
    ("in.words.delim",po::value<string>()->default_value(","), "delimiter used to seperate words.")

    ("out,o", po::value<string>()->default_value("similarity.csv"),"Output similarity matrix csv file") 

    ("vocabsize,V",  po::value<int>(), "Vocabulary size.")
    ("alpha", po::value<double>()->default_value(1.0),"histogram smoothing (needed for computing KL divergence)")
    ("threads",po::value<int>()->default_value(4),"Do it in parallel!")
    ("dfun", po::value<string>()->default_value("kl"),"kl=KLDivergence, cos=Cosine")
    ("expsim", po::value<bool>()->default_value(true),"return exp(-d^2/sigma^2)")
    ;
  return desc;
}

po::variables_map read_command_line(int argc, char* argv[], po::options_description& options){

  po::variables_map args;
  po::store(po::command_line_parser(argc, argv)
      .style(po::command_line_style::default_style ^ po::command_line_style::allow_guessing)
      .options(options)
      .run(), 
      args);
  po::notify(args);

  if(args.count("help")){
    cerr<<options<<endl;
    exit(0);
  }
  return args;
}


template<typename DFun>
vector<float> compute_distance(const vector<vector<float>>& observations, size_t i, const DFun& dist){
  vector<float> out(observations.size());
  for(size_t j=0;j<observations.size();++j){
    out[j]= dist(observations[i],observations[j]);
  }
  return out;
}

template<typename DFun>
vector<vector<float>> compute_distance_matrix(const vector<vector<float>>& observations, const DFun& dist){
  size_t n = observations.size();
  vector<vector<float>> dm;
//  KLDistance<float> dist;
  for(size_t i=0;i<n;++i){
    dm.push_back(compute_distance(observations,i,dist));
  }
  return dm;
}

template<typename DFun>
vector<vector<float>> parallel_compute_distance_matrix(const vector<vector<float>>& observations, int nthreads, const DFun& dist){
  size_t n = observations.size();
  vector<vector<float>> dm;

  //KLDistance<float> dist;

  for(size_t i=0;i<n; i+=nthreads){
//    dm.push_back(compute_distance(observations,i,dist));
    cerr<<static_cast<double>(i)/n*100.0 <<"% done               \r";
    vector<future<vector<float>>> futures;
    for(int t=i;t<min(i+nthreads, n); ++t){
      futures.push_back(async(launch::async, compute_distance<DFun>, observations,t,dist));
    }
    for(size_t j=0;j<futures.size();++j){
      dm.push_back(futures[j].get());
    }

  }
  cerr<<endl;

  return dm;
}





int main(int argc, char**argv){
  po::options_description options("Computes similarity matrix");
  load_rost_base_options(options);  
  auto args = read_command_line(argc, argv, options);

  int V = args["vocabsize"].as<int>();
  double alpha=args["alpha"].as<double>();


  vector<int> timestamps;
  vector<vector<float>> observations;

  cerr<<"Reading data from: "<<args["in.words"].as<string>()<<endl;
  csv_reader in(args["in.words"].as<string>(), args["in.words.delim"].as<string>()[0]); 

  if(args["in.format"].as<string>()=="words"){
    cerr<<"Input format: word list"<<endl;
  }
  else cerr<<"Input format: histogram"<<endl;

  vector<int> tokens = in.get();
  while(!tokens.empty()){
    vector<int> words(tokens.begin()+1, tokens.end());
    int timestamp = tokens[0];
    timestamps.push_back(timestamp);
    vector<float> observation;
    if(args["in.format"].as<string>()=="words"){
      vector<int> words_hist = histogram(words, V);
      observation = normalize(words_hist,alpha); 
    }
    else{
      //input is already a histogram
      observation = normalize(words,alpha); 
    }
    observations.push_back(observation);
    tokens = in.get();    
  }
  cerr<<"Read "<<timestamps.size()<<" timesteps."<<endl;
  cerr<<"Computing similarity matrix\n";
  vector<vector<float>> sim;

  chrono::time_point<chrono::system_clock> start, end;
  start = chrono::system_clock::now();

  bool dtype_cos=false;
  if(args["dfun"].as<string>()=="kl")
    dtype_cos=false;
  else
    dtype_cos=true;

  if(args["threads"].as<int>()>1)
    if(dtype_cos)
      sim = parallel_compute_distance_matrix(observations, args["threads"].as<int>(),CosDistance<float>());
    else
      sim = parallel_compute_distance_matrix(observations, args["threads"].as<int>(),KLDistance<float>());
  else 
    if(dtype_cos)
      sim = compute_distance_matrix(observations,CosDistance<float>());
    else
      sim = compute_distance_matrix(observations,KLDistance<float>());

  end = chrono::system_clock::now();
  cerr<<"computation time: "<< chrono::duration_cast<std::chrono::milliseconds>
                             (end-start).count()/ 1000.0<<"sec.";
//  vector<float> distances = flatten(sim);
  float sigma = stddev(flatten(sim));
  cerr<<"stddev of distance: "<<sigma<<endl;
   //dump the final output
  cerr<<"Writing similarity matrix to: "<<args["out"].as<string>()<<"\n";
  ofstream out(args["out"].as<string>());

  size_t n = timestamps.size();
  bool expsim = args["expsim"].as<bool>();
  for(size_t i=0;i<n; ++i){
    out<<timestamps[i];
    for(size_t j=0;j<n; ++j){
      float d = sim[i][j]+sim[j][i];
      if(expsim)
        out<<","<<exp(-d*d/sigma/sigma); //convert to similarity matrix values 0..1
      else
        out<<","<<d;
    }
    out<<endl;
  }

  return 0;
}



