#include "rost/rost.hpp"
#include "rost/program_options.hpp"
#include "rost/io.hpp"
#include <iostream>
#include <string>

#ifdef POSE_TXY
#define POSEDIM 3
typedef array<int,POSEDIM> pose_t;
typedef ROST<pose_t,neighbors<pose_t>, hash_container<pose_t> > ROST_t;
#endif

#ifdef POSE_T
#define POSEDIM 1
typedef array<int,POSEDIM> pose_t;
typedef ROST<pose_t,neighbors<pose_t>, hash_container<pose_t> > ROST_t;
#endif

#ifdef POSE_XY
#define POSEDIM 3
typedef array<int,POSEDIM> pose_t;
typedef ROST<pose_t,neighbors<pose_t>, hash_pose_ignoretime<pose_t> > ROST_t;
#endif

#ifndef POSEDIM
#error "Must define pose type."
#endif


int main(int argc, char*argv[]){

  po::options_description options("Topic modeling of data with 1 dimensional structure.");
  load_rost_base_options(options).add_options()
    ("in.topicmask",po::value<string>(),"Mask file for topics. Format is k lines of 0 or 1, where 0=> don't use the topic")
    ("add.to.topicmodel", po::value<bool>()->default_value(true), "Add the given initial topic labels to topic model. Only applicable when a topic model and topics are provided")
    ("out.intermediate.topics", po::value<bool>()->default_value(false), "output intermediate topics") 
    ("out.topics.online", po::value<string>()->default_value("topics.online.csv"), "topic labels computed online (only valid in online mode)") 
    ("in.position",   po::value<string>()->default_value(""),"Word position csv file.")
    ("out.position",   po::value<string>()->default_value("topics.position.csv"),"Position data for topics.")
    ("topicmodel.update", po::value<bool>()->default_value(true), "Update global topic model with each iteration")
    ("out.ppx.online",  po::value<string>()->default_value("perplexity.online.csv"), "Perplexity score for each timestep, immediately after it has been observed.")
    ("batch.maxtime", po::value<int>()->default_value(0), "Maximum time in milliseconds spent on processing the data. 0 implies no max time.")
    ("retime", po::value<bool>()->default_value(true), "If this option is given, then timestamp from the words is ignored, and a sequntial time is given to each timestep")
    ("grow.topics.size", po::value<bool>()->default_value(false),"Grow # topics using Chineese Restaurant Process (CRP)" )
    ("gamma", po::value<double>()->default_value(0.001),"Used to control topic growth rate using CRP. Only used when grow.topics.size=true")

    //("in.topics.position", po::value<string>()->default_value(""),"Initial topic label positions")    

    ;
  //("iter.write.topics", po::value<bool>()->default_value(false), "write intermediate topics")
  
  auto args = read_command_line(argc, argv, options);

  if(!args.count("vocabsize")){
    cerr<<"ERROR: Must specify vocabulary size."<<endl;
    exit(0);
  }
  
  pose_t G;
  G[0]=args["g.time"].as<int>();
  for(int i=1;i<G.size(); ++i) G[i]=args["g.space"].as<int>();

  int cell_space = args["cell.space"].as<int>(),
      cell_time = args["cell.time"].as<int>();

  ROST_t rost(args["vocabsize"].as<int>(),
              args["ntopics"].as<int>(),
              args["alpha"].as<double>(),
              args["beta"].as<double>(), 
              G);

  double g_sigma = args["g.sigma"].as<double>();
  rost.g_sigma = g_sigma;
  
  bool retime= args["retime"].as<bool>();


  Logger logger(args["logfile"].as<string>());

  logger
    <<"words="<< args["in.words"].as<string>()<<"\n"
    <<"init topics ="<<args["in.topics"].as<string>()<<"\n"
    <<"word posotion="<<args["in.position"].as<string>()<<"\n"
    <<"alpha="<< args["alpha"].as<double>()<<"\n"
    <<"beta="<< args["beta"].as<double>()<<"\n"
    <<"gamma="<< args["gamma"].as<double>()<<" (only used when grow.topics.size=true) \n"
    <<"grow.topics.size="<<(args["grow.topics.size"].as<bool>()?string("true"):string("false"))<< "\n"
    <<"g.space (neighborhood spatial extent in #cells)="<< args["g.space"].as<int>()<<"\n"
    <<"g.time (neighborhood temporal extent in #cells)="<< args["g.time"].as<int>()<<"\n"
    <<"cell.space (spatial width of a cell)="<< args["cell.space"].as<int>()<<"\n"
    <<"cell.time (temporal width of a cell)="<< args["cell.time"].as<int>()<<"\n"
    <<"K="<<  args["ntopics"].as<int>()<<"\n"
    <<"V="<< args["vocabsize"].as<int>()<<"\n"
    <<"iterations="<< args["iter"].as<int>()<<"\n";


  if(args["grow.topics.size"].as<bool>()){
    logger<<"CRP_Topics=true\n";    
    rost.enable_auto_topics_size();
    rost.gamma=args["gamma"].as<double>();
  }

  rost.update_global_model=args["topicmodel.update"].as<bool>();
  logger<<"topicmodel.update="<<rost.update_global_model<<"\n";


  if(args.count("in.topicmask"))
    load_topic_mask(rost,args["in.topicmask"].as<string>());
  

  //load the KxV topic model if specified
  bool add_to_topic_model=true;
  if(args["in.topicmodel"].as<string>()!=""){
    load_topic_model(rost,args["in.topicmodel"].as<string>());
    add_to_topic_model=args["add.to.topicmodel"].as<bool>();
  }
  

  ofstream out_ppx_iter("perplexity.iter.csv");
  //out_ppx<<"#iter,ppx_t1,ppx_t2,....ppx_tT,ppx_global"<<endl;

  //wordposes[cell_pose] -> {..word_poses..}
  std::map<pose_t, vector<pose_t> > wordposes; 
  vector<int> timestamps;

  ////////////////////////////////////////////  
  ////////////////////////////////////////////
  std::chrono::time_point<std::chrono::system_clock> start_time, end_time, word_add_time, last_word_add_time;
  int elapsed_time=0, total_elapsed_time=0;
  if(!args.count("online")){

    int maxtime =  args["batch.maxtime"].as<int>();
    logger<<"Processing words in batch mode.\n"; 


    tie(wordposes,timestamps) =  load_words( rost, 
					     args["in.words"].as<string>(), 
					     args["in.topics"].as<string>(), 
					     args["in.position"].as<string>(),
					     args["cell.time"].as<int>(), 
					     args["cell.space"].as<int>(),
					     add_to_topic_model,
					     retime
);

    logger<<"Created "<<rost.cells.size()<<" cells\n";
    logger<<"Writing position file:"<<args["out.position"].as<string>()<<"\n";
    write_poses(rost,args["out.position"].as<string>(),wordposes);

    maxtime = maxtime*timestamps.size(); //scale the processing time with number of timesteps

    for(int i=0; i<args["iter"].as<int>(); ++i){
      cerr<<"iter: "<<i;
      start_time = chrono::system_clock::now();
      parallel_refine(&rost, args["threads"].as<int>());
      end_time = chrono::system_clock::now();
      elapsed_time = std::chrono::duration_cast<std::chrono::milliseconds>
                             (end_time-start_time).count();
      total_elapsed_time +=elapsed_time;
      cerr<<", iter_time:"<<elapsed_time/1000.0<<"sec., total_time:"<<total_elapsed_time/1000.0<<"sec.          \r";

      
      if(maxtime>0 && total_elapsed_time >maxtime) break;

      //write perplexity
      if((args["ppx.rate"].as<int>() >0) && 
	       (i%args["ppx.rate"].as<int>()==0 || i==(args["iter"].as<int>() -1))) 
      {
        cerr<<"\nComputing perplexity..."<<endl;

        double global_ppx = rost.perplexity(true); //don't recompute cell ppx
        logger<<"iteration="<<i<<" perplexity="<<global_ppx<<"\n";
        out_ppx_iter<<global_ppx<<endl;

        if(args["out.intermediate.topics"].as<bool>()){
          cerr<<"Writing intermediate topics results"<<endl;
          ofstream out_topics(string("topics_")+to_string(i)+".csv");

          for(int d = 0; d< rost.C; ++d){
            pose_t pose = rost.cell_pose[d];
            vector<int> topics = rost.cells[d]->Z;//rost.get_topics_for_pose(rost.cell_pose[d]);
            out(out_topics, wordposes[pose], topics);
          }
          out_topics.close();
        }

      }
    }
  
  }
  ///////////////////////////////////
  ////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////////
  //online processing
  else{
    cerr<<"Processing words online."<<endl;
    atomic<bool> stop;
    stop.store(false);
    //auto workers =  parallel_refine_online(&rost, args["tau"].as<double>(), args["threads"].as<int>(), &stop);
    //auto workers =  parallel_refine_online_exp(&rost, args["tau"].as<double>() , args["threads"].as<int>(), &stop);
    auto workers =  parallel_refine_online_exp_beta(&rost, args["tau"].as<double>(),args["refine.weight.local"].as<double>(),args["refine.weight.global"].as<double>() , args["threads"].as<int>(), &stop);

    ofstream out_topics(args["out.topics.online"].as<string>());
    ofstream out_onppx(args["out.ppx.online"].as<string>());

    csv_reader in_words(args["in.words"].as<string>(), args["in.words.delim"].as<string>()[0]); 
    csv_reader in_poses(args["in.position"].as<string>());
    bool have_poses= (args["in.position"].as<string>() != "");
    ofstream outf_poses;

    vector<int> pose_line, word_line = in_words.get();
    if(have_poses){
      pose_line=in_poses.get();
      outf_poses.open(args["out.position"].as<string>());
    }

    int ti=0;
    int iter_process_time=args["online.mint"].as<int>();

    ROST_t::word_data_t all_cell_data;
    set<pose_t> cposes_at_current_time;
    //cerr<<"adasdasd"<<endl;
    bool first=true;
    start_time = chrono::system_clock::now();
    last_word_add_time = chrono::system_clock::now();
      
    while(!word_line.empty()){

      

      int W = word_line.size()-1; //number of words in this time step
      int current_time = word_line[0]; //current time stamp
      int current_cell_time = retime?ti:(current_time/cell_time); //current time for the cell

      end_time = chrono::system_clock::now();
      elapsed_time = std::chrono::duration_cast<std::chrono::milliseconds> (end_time-start_time).count();
      start_time=end_time;
      cerr<<"i:"<<ti<<" T:"<<current_time<<"  W:"<<W<<" Titer:"<<elapsed_time;

      timestamps.push_back(current_time);
      if(have_poses && pose_line[0] != current_time){ 
        cerr<<"ERROR. word position timestamps are not synced"<<endl;
        break;
      } 

      //collect words into cells
      //start_time = chrono::system_clock::now();
      cposes_at_current_time.clear();
      for(int i=0;i<W; ++i){
        pose_t cpose; cpose[0]=current_cell_time ;
        pose_t wpose; wpose[0]=current_time;
        if(have_poses){
          for(size_t d=1;d<cpose.size();++d){
            cpose[d]=pose_line[ (cpose.size()-1)*i+d ]/cell_space;
            wpose[d]=pose_line[ (cpose.size()-1)*i+d ];
          }
        }
        cposes_at_current_time.insert(cpose);
        get<0>(all_cell_data[cpose]).push_back(word_line[i+1]);//words
        //get<2>(all_cell_data[cpose]).push_back(wpose);//word poses


        wordposes[cpose].push_back(wpose);
      }

      //push the observed data into ROST
      for(auto cpose: cposes_at_current_time){
        vector<int>& words = get<0>(all_cell_data[cpose]);
	//cerr<<"Adding cpose: "<<cpose[0]<<","<<cpose[1]<<","<<cpose[2]<<endl;
        rost.add_observation(cpose,words.begin(), words.end());
      }


      //sleep for some time to allow the refinement process 
      word_add_time = chrono::system_clock::now();
      elapsed_time = std::chrono::duration_cast<std::chrono::milliseconds> (word_add_time - last_word_add_time).count();
      this_thread::sleep_for(chrono::milliseconds(max<int>(0,iter_process_time-elapsed_time)));      
      last_word_add_time= chrono::system_clock::now();

      //output the topics from the previous time step.
      vector<int>topics;
      vector<pose_t> poses;
      for(pose_t cpose: cposes_at_current_time){
	vector<int> ctopics = rost.get_ml_topics_for_pose(cpose,true);
	topics.insert(topics.end(),ctopics.begin(),ctopics.end());
	poses.insert(poses.end(), wordposes[cpose].begin(), wordposes[cpose].end());
      }
      out(out_topics,poses[0][0],topics);
      if(have_poses)out_poses(outf_poses,poses);
      double ppx=rost.time_perplexity(current_cell_time);
      out_onppx<<current_time<<","<<ppx<<endl;
      cerr<<" PPX:"<<ppx<<endl;

      //read the next line of words and poses
      word_line = in_words.get();    
      if(have_poses)pose_line=in_poses.get(); 
      ti++;
    }
    cerr<<endl;
    cerr<<"Reached EOF. Terminating."<<endl;
    stop.store(true);
    for(auto t:workers){
      t->join();
    }
  }


  cerr<<endl;
  logger<<"Writing topics to:"<<args["out.topics"].as<string>()<<"\n";

  write_topics(rost,args["out.topics"].as<string>(), wordposes, false);


  logger<<"Computing and writing maximum likelihood topics to: "<<args["out.topics.ml"].as<string>()<<"\n";
  write_topics(rost,args["out.topics.ml"].as<string>(), wordposes, true);

  logger<<"Writing perplexity for each time step to:" <<args["ppx.out"].as<string>()<<"\n";  
  if(retime)
    write_time_perplexity(rost,args["ppx.out"].as<string>(), timestamps,true);      
  else
    write_time_perplexity(rost,args["ppx.out"].as<string>(), timestamps,cell_time,true);      
  


  if(args["out.topicmodel"].as<string>()!=""){
    logger<<"Writing topicmodel to:" <<args["out.topicmodel"].as<string>()<<"\n";  
    save_topic_model(rost,args["out.topicmodel"].as<string>());
  }

  return 0;
}
