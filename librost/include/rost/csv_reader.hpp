#ifndef ROST_CSV_READER_HPP
#define ROST_CSV_READER_HPP
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <regex>
#include <cassert>
//#include <boost/spirit/include/qi.hpp>
using namespace std;
//namespace qi = boost::spirit::qi;
//namespace ascii = boost::spirit::ascii;

struct csv_reader{
  istream* stream;
  string line;  
  //char buffer[1024*1024];
  char delim;
  bool delete_stream;
  csv_reader(string filename, char delim_=','):stream(NULL),delim(delim_)
  {
    if(filename=="-"){
      filename = "/dev/stdin";
      delete_stream=false;
    }
    if(filename != ""){
      stream = new ifstream(filename);
      delete_stream=true;
    }
    //    stream->rdbuf()->pubsetbuf(buffer,1024*1024);
  }  

  struct row{
    stringstream ss;      
    bool done;
    string::iterator it_begin, it_end;
    /*    row(const sregex_token_iterator& begin_, const sregex_token_iterator& end_):it_begin(begin_),it_end(end_),done(false){
      if(it_begin == it_end)
        done=true;
        }*/
    row(const string::iterator& begin_, const string::iterator& end_):it_begin(begin_),it_end(end_),done(false){
      if(it_begin == it_end)
        done=true;
    }
    row():done(true){it_begin=it_end;}
    row(const row& r){it_begin = r.it_begin; it_end = r.it_end; done = r.done;}

    bool empty(){
      return it_begin == it_end;
    }

    string::iterator column_increment(){
      for(;it_begin != it_end; ++it_begin){
        if(*it_begin == ','){
          *it_begin=0;
          it_begin++;
          break;
        }
      }
      return it_begin;
    }

    inline int get_int(){
      assert(!empty());
      auto tbegin = it_begin, tend = column_increment();
      return atoi(&(*tbegin));
    }
    inline float get_float(){
      assert(!empty());
      auto tbegin = it_begin, tend = column_increment();
      return atof(&(*tbegin));
    }

    vector<int> get_vector_int(){
      vector<int> v;
      while(!empty()){
          v.push_back(get_int());
      }
      return v;
    }

    vector<float> get_vector_float(){
      vector<float> v;
      while(!empty()){
          v.push_back(get_float());
      }
      return v;
    }
    
  };

  vector<int> get(){
    row r = next_row();
    return r.get_vector_int();
  }
  vector<vector<int>> get_all_int(){
    vector<int> row = get();
    vector<vector<int>> rows;
    while(!row.empty()){
      rows.push_back(row);
      row = get();
    }
    return rows;
  }


  vector<float> get_floats(){
    row r = next_row();
    return r.get_vector_float();
  }



  row next_row(){
    line.clear();
    while(true){
      getline(*stream,line);
      if(*stream){
        if(!line.empty()){
          if(line[0]=='#')
            continue;
          else{
            return row(line.begin(),line.end());
          }
        }
      }
      else{
        return row();
      }
    }
  }

  ~csv_reader(){
    if(delete_stream && stream !=NULL){
      delete stream;
      stream=0;
    }
  }
};




#endif
