#include <array>
#include <vector>
#include <functional>
#include <algorithm>
#include <unordered_map>
#include <random>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cassert>
#include <mutex>
#include <memory>
#include <thread>
#include <iterator>
#include <atomic>
#include <set>
#include <map>
#include <condition_variable>

#include "rost/refinery.hpp"
#include "rost/word_reader.hpp"
#include "rost/markov.hpp"
using namespace std;


//maps the v to [-N,N)
template<typename T>
T standardize_angle( T v, T N=180){  
  T r = v%(2*N);
  r = r >= N ? r-2*N: r; 
  r = r < -N ? r+2*N: r;
  return r;
}

template<typename Pose>
int pose_distance(const Pose& p1, const Pose& p2){
  int d=0;
  for(size_t i=0;i<p1.size(); ++i){
    d+= std::abs(p1[i] - p2[i]);
  }
  return d;
}
template<typename Pose>
struct pose_equal{
  bool operator()(const Pose& l, const Pose& r)const{
    for(size_t i=0;i<l.size(); ++i){
      if(l[i] != r[i]) return false;
    }
    return true;
  }
};

template<typename Pose>
struct pose_equal_ignoretime{
  bool operator()(const Pose& l, const Pose& r)const{
    for(size_t i=1;i<l.size(); ++i){
      if(l[i] != r[i]) return false;
    }
    return true;
  }
};

template<>
int pose_distance<int>(const int& p1, const int& p2){
  return std::abs(p1-p2);
}
//neighbors specialization for a pose of array type
template<typename Array>
struct neighbors_hypercube {
  //  const int depth;
  Array depth;
  size_t neighborhood_size;
  neighbors_hypercube( int depth_){
    depth.fill(depth_);
    //neighborhood_size=depth.size()*2*depth[0];
    neighborhood_size=1;
    for(auto d: depth){
      neighborhood_size *= (d*2+1);
    }
    //neighborhood_size-=1;
  }
  neighbors_hypercube( const Array& depth_):depth(depth_){
    neighborhood_size=1;
    depth=depth_;
    for(auto d: depth){
      neighborhood_size *= (d*2+1);
    }
    //neighborhood_size-=1;
  }
  vector<Array> operator()(const Array& o) const{

    vector<Array> neighbor_list(neighborhood_size);

    for(size_t i=0; i<neighbor_list.size(); ++i){
      int j=i;
      Array& pose = neighbor_list[i];
      for(size_t d=0;d<depth.size(); ++d){
        auto width=(depth[d]*2+1);
        pose[d]=(j%width - depth[d]);
        j=j/width;
      }
    }
    return neighbor_list;
  }
};


//neighbors specialization for a pose of array type
template<typename Array>
struct neighbors {
  //  const int depth;
  Array depth;
  size_t neighborhood_size;
  neighbors( int depth_){
    depth.fill(depth_);
    neighborhood_size=depth.size()*2*depth[0];
  }
  neighbors( const Array& depth_):depth(depth_){
    neighborhood_size=0;
    for(auto d:  depth){
      neighborhood_size += 2*d;
    }
  }
  vector<Array> operator()(const Array& o) const{

    vector<Array> neighbor_list(neighborhood_size, o);
    
    auto outit = neighbor_list.begin();
    for(size_t i=0; i<o.size(); ++i){
      for(int d = 0; d<depth[i]; ++d){
        (*outit++)[i] += d+1;
        (*outit++)[i] -= d+1;
      }
    }
    return neighbor_list;
  }
};

template<>
struct neighbors<int> {
  int depth;
  neighbors(int depth_):depth(depth_){}
  vector<int> operator()(int o) const{
    vector<int> neighbor_list(2*depth,o);
    auto i = neighbor_list.begin();
    for(int d=1;d<=depth; ++d){
      (*i++)+=d;
      (*i++)-=d;
    }
    return neighbor_list;
  }
};

//Spatial hash function
template<typename T>
struct hash_container{
  hash<typename T::value_type> hash1;
  size_t operator()(const T& pose) const {
    size_t h(0);
    for(size_t i=0;i<pose.size(); ++i){
      if(sizeof(size_t)==4){
        h = (h<<7) | (h >> 25);
      }
      else{
        h = (h<<11) | (h >> 53);
      }
      h = hash1(pose[i]) ^ h;
    }
    return h;
  }
};

//this hash functor ignores the first dim of the pose (time)
//while computing the hash 
template<typename T>
struct hash_pose_ignoretime{
  hash<typename T::value_type> hash1;
  size_t operator()(const T& pose) const {
    size_t h(0);
    for(size_t i=1;i<pose.size(); ++i){
      if(sizeof(size_t)==4){
        h = (h<<7) | (h >> 25);
      }
      else{
        h = (h<<11) | (h >> 53);
      }
      h = hash1(pose[i]) ^ h;
    }
    return h;
  }
};

//outer product of dist with itself.
vector<vector<int>> cooccurence_counts(vector<int>& dist){
  vector<vector<int>> cc(dist.size(),vector<int>(dist.size(),0));
  for(size_t i=0;i<dist.size();++i){
    for(size_t j=0;j<dist.size(); ++j){
      cc[i][j]=dist[i]*dist[j];
    }
  }
  return cc;
}

/// All observed data is stored in Cell structs. Each cell contains
/// all observed word and their topic labels that have the same saptiotemporal
/// location. A Cell is connected to its spatiotemporal neighbors
struct Cell{
  size_t id;
  vector<size_t> neighbors;
  vector<int> W; //word labels
  vector<int> Z; //topic labels
  //vector<int> nW; //distribution/count of W
  vector<int> nZ; //distribution/count of Z
  vector<vector<int>> nZZ; //couccrance counts of Z (KxK matrix)

  vector<float> W_perplexity; // perplexity of each word
  mutex cell_mutex;
  vector<mutex> Z_mutex;
  double perplexity;
  Cell(size_t id_, size_t vocabulary_size, size_t topics_size):
    id(id_),
    //nW(vocabulary_size, 0),
    nZ(topics_size, 0),
    Z_mutex(topics_size)
  {
    neighbors.reserve(24); //TOFIX: access to neighbors list should be protected using mutex. 
  }
  vector<int> get_topics(){
    lock_guard<mutex> lock(cell_mutex);
    return Z;
  }
  pair<int, int> get_wz(int i){
    cell_mutex.lock();
    auto r=make_pair(W[i],Z[i]);
    cell_mutex.unlock();
    return r;
  }
  void relabel(size_t i, int z_old, int z_new){
    if(z_old == z_new)
      return;
    lock(Z_mutex[z_old], Z_mutex[z_new]);
    Z[i]=z_new;
    nZ[z_old]--;
    nZ[z_new]++;    
    Z_mutex[z_old].unlock();  Z_mutex[z_new].unlock();
  }
  void shrink_to_fit(){
    neighbors.shrink_to_fit();
    W.shrink_to_fit();
    Z.shrink_to_fit();
    //nW.shrink_to_fit();
    nZ.shrink_to_fit();
    //Z_mutex.shrink_to_fit();
  }
  void forget(){
    W.clear();
    Z.clear();
    for(auto &z:nZ)z=0;
  }
  void update_cooccurence_counts(){
    nZZ = cooccurence_counts(nZ);
  }
};

template<typename T>
struct scaled_plus{
  T scale;
  scaled_plus(const T& scale_):scale(scale_){}
  T operator()(const T& v1, const T& v2) const{
    return v1 + v2/scale;
  }
};

template<typename PoseT, 
         typename PoseNeighborsT=neighbors<PoseT>, 
         typename PoseHashT=hash_container<PoseT>,
         typename PoseEqualT=pose_equal<PoseT>
 >
struct ROST{
  typedef PoseT pose_t;
  typedef typename PoseT::value_type pose_dim_t;
  /*typedef std::map<pose_t, 
                  tuple<
                    vector<int>, //words
                    vector<int>, //topics
                    vector<pose_t> //exact poses
                  >
                > word_data_t;*/
  typedef unordered_map<PoseT, //cell_pose
                        tuple<
                          vector<int>, //words
                          vector<int>, //topics
                          vector<pose_t> //word poses
                        > , 
                        PoseHashT, 
                        PoseEqualT
                      > word_data_t;

  PoseNeighborsT neighbors; //function to compute neighbors
  PoseHashT pose_hash;
  unordered_map<PoseT, size_t , PoseHashT, PoseEqualT> cell_lookup;

  map<pose_dim_t, set<PoseT> > poses_for_time; // stores list of poses for each time step in sorted order

  vector<shared_ptr<Cell>> cells;
  vector<PoseT> cell_pose;
  mutex cells_mutex;     //lock for cells, since cells can grow in size
  size_t V, K, C;        //vocab size, topic size, #cells
  float beta,gamma;
  vector<float> alpha,gamma_i; 
  float g_sigma; //neighborhood decay
  mt19937 engine;
  parallel_random random;
  //ranlux24_base engine;
  //minstd_rand0 engine;

  uniform_int_distribution<int> uniform_K_distr;
  vector<vector<int>> nZW; //nZW[z][w] = freq of words of type w in topic z
  vector<int> weight_Z;
  vector<vector<int>> nZZ; //nZZ[k1][k2] = number of times we have seen topic k1 and k2 together in a cell
  vector<int> nZZ_weight; //marginalized nZZ;
  vector<mutex> global_Z_mutex;
  mutex global_ZZ_mutex;
  atomic<size_t> refine_count; //number of cells refined till now;
  atomic<size_t> activity; //number of threads actively using the topic model
  vector<int> mask_Z; //topic mask 
  //variables to handle pause/unpausing the system
  bool is_paused; //pause the topic model
  condition_variable pause_condition;
  mutex pause_mutex, gamma_update_mutex;
  bool fixed_k; //is true if LDA, false if HDP to grow topics
  int new_k; //id of the next new topic(only applicable when fixed_k==false)
  bool update_global_model; //if true, with each iteration teh global topic model is updated along with the local cell topic model
                            // if false, only the local cell model is updated
  bool use_topic_cooccurence; //if true then topic priors include information of cooccuring topics
  ROST(size_t V_, size_t K_, double alpha_, double beta_, const PoseNeighborsT& neighbors_ = PoseNeighborsT(), const PoseHashT& pose_hash_ = PoseHashT()):
    neighbors(neighbors_),
    pose_hash(pose_hash_),
    cell_lookup(10000000, pose_hash),
    V(V_), K(K_), C(0),
    beta(beta_), alpha(K,alpha_), gamma(1.0), gamma_i(K,1.0),
    g_sigma(0.5),
    uniform_K_distr(0,K-1),
    nZW(K,vector<int>(V,0)),
    weight_Z(K,0),
    nZZ(K,vector<int>(K,0)),
    nZZ_weight(K,0),
    mask_Z(K,1),
    global_Z_mutex(K),
    refine_count(0),
    is_paused(false),
    fixed_k(true),
    update_global_model(true),
    use_topic_cooccurence(false)
  {
  }
    

  double perplexity(bool recompute=false){
    double seq_ppx=0;
    int n_words=0;
    //locks the cells vector so that it doesnt reallocate while accessing it.
    //lock_guard<mutex> lock(cells_mutex);  
    for(auto& cell : cells){
      if(recompute)
        estimate(*cell,true);
      n_words += cell->Z.size();
      seq_ppx+=cell->perplexity;
    }
    return exp(-seq_ppx/n_words);
  }

  double perplexity(const PoseT& pose, bool recompute=false){
    double seq_ppx=0;
    int n_words=0;
    //locks the cells vector so that it doesnt reallocate while accessing it.
    auto cell = get_cell(cell_lookup[pose]);
    if(recompute)
      estimate(*cell,true);
    n_words += cell->Z.size();
    seq_ppx+=cell->perplexity;

    return exp(-seq_ppx/n_words);
  }

  double time_perplexity(pose_dim_t t, bool recompute=false){
    double seq_ppx=0;
    int n_words=0;
    //locks the cells vector so that it doesnt reallocate while accessing it.
    //lock_guard<mutex> lock(cells_mutex);  
    for(auto& pose : poses_for_time[t]){
      auto cell = get_cell(cell_lookup[pose]);
      if(recompute)
        estimate(*cell,true);
      n_words += cell->Z.size();
      seq_ppx+=cell->perplexity;
    }
    return exp(-seq_ppx/n_words);  
  }

  vector<float> word_perplexity(const PoseT& pose, bool recompute=false){
    double seq_ppx=0;
    int n_words=0;

    auto cell = get_cell(cell_lookup[pose]);
    if(recompute)
      estimate(*cell,true);

    return cell->W_perplexity;
  }

  /*  double mean_topic_perplexity(const PoseT& pose){
    double r=0;
    int n_words=0;
    int W=accumulate(weight_Z.begin(), weight_Z.end(),0)+K;
    auto cell = get_cell(cell_lookup[pose]);
    for(size_t i=0;i<cell->nZ.size();++i){
      r += -log(double(weight_Z[i]+1)/double(W)) * double(cell->nZ[i]+1);
    }
    n_words += cell->Z.size()+K;

    return r/n_words;
  }
  */

  double topic_perplexity(const PoseT& pose){
    double seq_ppx=0;
    int n_words=0;
    double alpha_total = accumulate(alpha.begin(), alpha.end(),0.0);
    double W=accumulate(weight_Z.begin(), weight_Z.end(),0)+alpha_total;
    //locks the cells vector so that it doesnt reallocate while accessing it.
    auto cell = get_cell(cell_lookup[pose]);
    for(size_t i=0;i<cell->nZ.size();++i){
      seq_ppx -= double(cell->nZ[i])*log(double(max<int>(0,weight_Z[i])+alpha[i])/double(W));
      assert(seq_ppx == seq_ppx);
    }
    n_words = cell->Z.size();
    if(n_words == 0) 
      return 1.0;
    else
      return exp(seq_ppx/n_words);
  }


  //compute topic perplexity given a topic distribution
  // returns the geometric mean of 1/P(topics[i] | weight_Z), where weight_Z is the current global topic distribution
  double cell_perplexity_topic(const vector<int>& nZ){
    double seq_ppx=0;
    int n_words=0;
    double alpha_total = accumulate(alpha.begin(), alpha.end(),0.0);
    double W=accumulate(weight_Z.begin(), weight_Z.end(),0)+alpha_total;
    //locks the cells vector so that it doesnt reallocate while accessing it.
    for(size_t i=0;i<K;++i){
      seq_ppx -= double(nZ[i])*log(double(weight_Z[i]+alpha[i])/W);
    }
    n_words += accumulate(nZ.begin(), nZ.end(), 0);
    if(n_words ==0 ) 
      return 1.0;
    else
      return exp(seq_ppx/n_words);
  }

  // returng the geometric mean of 1/P(word[i] | nZW, nZ), where nZW is the current topic model (KxV), and nZ is the topic distribution prior
  // nZ could be a local or global topic distribution.
  double cell_perplexity_word(const vector<int>& words, const vector<int>& nZ){
    int weight_c = accumulate(nZ.begin(), nZ.end(),0);
    double alpha_total = accumulate(alpha.begin(), alpha.end(),0.0);

    double ppx_sum=0;
    for(const int& w: words){
      double p_word=0;
      for(size_t k=0; k<nZ.size(); ++k){
        p_word +=  (nZW[k][w]+beta)/(weight_Z[k] + beta*V)  * (nZ[k]+alpha[k])/(weight_c+alpha_total);
      }
      ppx_sum += log(p_word);
    }
    return exp(-ppx_sum/words.size());
  }

  vector<float> word_topic_perplexity(const PoseT& pose){
    double seq_ppx=0;
    int W=accumulate(weight_Z.begin(), weight_Z.end(),0)+K;
    //locks the cells vector so that it doesnt reallocate while accessing it.
    auto cell = get_cell(cell_lookup[pose]);
    size_t n = cell->Z.size();
    vector<float> result(n);
    for(size_t i=0;i<cell->Z.size();++i){
      result[i]=W/double(weight_Z[i]+1);
    }
    return result;
  }


  void set_topic_model(const vector<int>& new_nZW, const vector<int>& new_weight_Z){
    assert(new_nZW.size() == K*V);
    assert(new_weight_Z.size() == K);

    for(int k=0;k<K; ++k){
      copy(new_nZW.begin()+k*V, new_nZW.begin()+(k+1)*V, nZW[k].begin());
    }
    weight_Z = new_weight_Z;
  }

  //pauses the refinement adding new data
  void pause(bool p){

    cerr<<"Setting pause="<<p<<endl;
    {
      lock_guard<mutex> lock(pause_mutex);
      is_paused = p;
    }
    cerr<<"Notifying all"<<endl;
    pause_condition.notify_all();
    cerr<<"Notification sent"<<endl;
  }

  void wait_till_unpaused(){
    if(!is_paused) return;
    unique_lock<mutex> lock(pause_mutex);
    //wait till pause_condition receives a notification
    //bool & p = is_paused;
    //pause_condition.wait(lock, [&p](){return ! p;})
    while(is_paused){
      cerr<<"Waiting to unpause"<<endl;
      pause_condition.wait(lock);
      cerr<<"maybe unpaused"<<endl;
    }
    cerr<<"Unpaused"<<endl;
  }

  decltype(weight_Z) get_topic_weights(){
    return weight_Z;
  }

  //returns the KxV topic-word distribution matrix
  decltype(nZW) get_topic_model(){
    return nZW;
  }

  //topics in the cell for the given pose
  vector<int> get_topics_for_pose(const PoseT& pose){
    //lock_guard<mutex> lock(cells_mutex);
    auto cell_it = cell_lookup.find(pose);
    if(cell_it != cell_lookup.end()){ 
      auto c = get_cell(cell_it->second);
      return c->Z;
    }
    else
      return vector<int>();
  }

  //compute maximum likelihood estimate for topics in the cell for the given pose
  vector<int> get_ml_topics_for_pose(const PoseT& pose, bool update_ppx=false){
    //lock_guard<mutex> lock(cells_mutex);
    auto cell_it = cell_lookup.find(pose);
    if(cell_it != cell_lookup.end()){ 
      auto c = get_cell(cell_it->second);
      lock_guard<mutex> lock(c->cell_mutex);
      return estimate(*c,update_ppx);
    }
    else
      return vector<int>();
  }

  //compute maximum likelihood estimate for topics in the cell for the given pose
  tuple<vector<int>,double> get_ml_topics_and_ppx_for_pose(const PoseT& pose){
    vector<int> topics;
    double ppx =0;
    auto cell_it = cell_lookup.find(pose);
    if(cell_it != cell_lookup.end()){ 
      auto c = get_cell(cell_it->second);
      lock_guard<mutex> lock(c->cell_mutex);
      topics = estimate(*c,true);
      ppx = c->perplexity;
    }
    return make_tuple(topics,ppx);
  }

  tuple<vector<int>,double> get_topics_and_ppx_for_pose(const PoseT& pose){
    vector<int> topics;
    double ppx =0;
    auto cell_it = cell_lookup.find(pose);
    if(cell_it != cell_lookup.end()){ 
      auto c = get_cell(cell_it->second);
      topics = c->Z;
      ppx = c->perplexity;
    }
    return make_tuple(topics,ppx);
  }

  shared_ptr<Cell> get_cell(const PoseT& pose){
    //cerr<<"GetCell:"<<cid<<endl;
    lock_guard<mutex> lock(cells_mutex);
    auto it = cell_lookup.find(pose);
    assert(it != cell_lookup.end());
    return cells[it->second];
  }

  shared_ptr<Cell> get_cell(size_t cid){
    //cerr<<"GetCell:"<<cid<<endl;
    lock_guard<mutex> lock(cells_mutex);
    return cells[cid];
  }

  size_t get_refine_count(){
    return refine_count.load();
  }
  size_t num_cells(){
    return C;
  }

  void update_cooccurence_counts(const vector<vector<int>>& delta, int m=1){
    cerr<<"Updating rost cooccurence_counts"<<endl;
    lock_guard<mutex> lock(global_ZZ_mutex);
    for(int i=0; i<K; ++i){
      nZZ_weight[i]=0;
      for(int j=0; j<K; ++j){
        nZZ[i][j] += m*delta[i][j];
        nZZ_weight[i] += nZZ[i][j];
      }
    }
  }
  void update_cooccurence_counts(const vector<vector<int>>& delta_sub, const vector<vector<int>>& delta_add){
    cerr<<"Updating rost cooccurence_counts"<<endl;
    lock_guard<mutex> lock(global_ZZ_mutex);
    for(int i=0; i<K; ++i){
      nZZ_weight[i]=0;
      for(int j=0; j<K; ++j){
        nZZ[i][j] += (delta_add[i][j] - delta_sub[i][j]);
        nZZ_weight[i] += nZZ[i][j];
      }
    }
  }

  void add_count(int w, int z, int c=1){
    lock_guard<mutex> lock(global_Z_mutex[z]);
    nZW[z][w]+=c;
    weight_Z[z]+=c;
  }

  void relabel(int w, int z_old, int z_new){
    //cerr<<"lock: "<<z_old<<"  "<<z_new<<endl;
    if(z_old == z_new) return;

    lock(global_Z_mutex[z_new], global_Z_mutex[z_old]);

    nZW[z_old][w]--;
    weight_Z[z_old]--;
    nZW[z_new][w]++;
    weight_Z[z_new]++;

    global_Z_mutex[z_old].unlock();
    global_Z_mutex[z_new].unlock();
    //cerr<<"U:"<<z_old<<","<<z_new<<endl;
  }

  void shuffle_topics(){
    for(auto &c : cells){
      lock_guard<mutex> lock(c->cell_mutex);
      for(size_t i=0;i<c->Z.size(); ++i){
        int z_old = c->Z[i];
        int w = c->W[i];
        int z_new=uniform_K_distr(engine);
        c->nZ[z_old]--;
        c->nZ[z_new]++;
        nZW[z_old][w]--;
        nZW[z_new][w]++;
        weight_Z[z_old]--;
        weight_Z[z_new]++;
        c->Z[i]=z_new;
      }
    }
  }

  template<typename WordContainer>
  void add_observation(const PoseT& pose, const WordContainer& words){
    cerr<<"add_obsercation with no topics"<<endl;
    add_observation(pose,words.begin(), words.end(), true, words.end(), words.end());
  }
  template<typename WordIt>
  void add_observation(const PoseT& pose, WordIt word_begin, WordIt word_end, bool update_topic_model=true){
    WordIt wi;
    add_observation(pose,word_begin,word_end,update_topic_model,word_end,word_end);    
  }
  template<typename WordIt>
  void add_observation(const PoseT& pose, WordIt word_begin, WordIt word_end, bool update_topic_model, WordIt topic_begin, WordIt topic_end){
    auto cell_it = cell_lookup.find(pose);
    bool newcell = false;
    shared_ptr<Cell> c;
    if(cell_it == cell_lookup.end()){
      c = make_shared<Cell>(C,V,K);
      cells_mutex.lock();
      cells.push_back(c);
      cell_pose.push_back(pose);
      cells_mutex.unlock();

      c->cell_mutex.lock();
      //add neighbors to the cell
      for(auto& g : neighbors(pose)){
        auto g_it = cell_lookup.find(g);
        if(g_it != cell_lookup.end()){
          auto gc = get_cell(g_it->second);
          //  cerr<<gc->id<<" ";
          gc->neighbors.push_back(c->id);
          c->neighbors.push_back(gc->id);
        }
      }
      //      cerr<<endl;
      cell_lookup[pose]=c->id;
      newcell=true;
    }
    else{
      c = get_cell(cell_it -> second);
      c->cell_mutex.lock();
    }



    bool havetopics= (topic_begin != topic_end);
    if(havetopics)cerr<<"Using topics"<<endl;
    //vector<int> active_K(K,0);
    //for(size_t i=0;i<K; ++i){
    //  active_K[i]= (weight_Z[i]>0?1:0);
    //}

    //do the insertion
    for(auto wi = word_begin; wi!=word_end; ++wi){
      int w = *wi;
      c->W.push_back(w);
      //generate random topic label
      
      int z;
      if(havetopics){
        z = *topic_begin; topic_begin++;
      }
      else{
        //z = uniform_K_distr(engine);
        z = random.category_pdf(gamma_i.begin(), gamma_i.end());
        //z = random.category_pdf(active_K.begin(), active_K.end());
      }
      assert(z<K);
      c->Z.push_back(z);
      //update the histograms
      c->nZ[z]++; 
      if(update_topic_model && update_global_model) add_count(w,z);
      if(z == new_k) update_gamma();
    }

    c->shrink_to_fit();

    if(use_topic_cooccurence){
      c->update_cooccurence_counts();
      update_cooccurence_counts(c->nZZ);
    }

    if(newcell){
      C++;
    }
    poses_for_time[pose[0]].insert(pose);

    c->cell_mutex.unlock();
  }
  
  //returns the topic distribution in the neighborhood of a cell
  vector<int> neighborhood(Cell& c){
   vector<int> nZg(K,0); //topic distribution over the neighborhood (initialize with the cell)

    //accumulate topic histogram from the neighbors
    for(auto gid: c.neighbors){
      if(gid <C && gid != c.id){
        auto g = get_cell(gid); 
        auto dist = pose_distance(cell_pose[gid], cell_pose[c.id]);
//        int scale = (1<<dist);
          float scale = pow(g_sigma,dist);
//        int scale=1;
        for(size_t k=0; k<nZg.size(); ++k){
          nZg[k]+=ceil(g->nZ[k]*scale);
        }
        //transform(g->nZ.begin(), g->nZ.end(), nZg.begin(), nZg.begin(), [&](int x, int y){return x/scale +y;} );
      }
    }
    transform(c.nZ.begin(), c.nZ.end(), nZg.begin(), nZg.begin(), plus<int>());

    if(use_topic_cooccurence){
      vector<int> nZg_cooccurence(nZg.size(),0);
    }    
    return nZg;
  }

  bool forget(int cid=-1){
    if(cid<0)
      cid=random.uniform(0,C-1);
    auto c = get_cell(cid);
    lock_guard<mutex> lock(c->cell_mutex);
    for(size_t i=0;i<c->W.size(); ++i){
      add_count(c->W[i],c->Z[i],-1);
    }
    c->forget();
    c->shrink_to_fit();
    return true;
  }

 

  //update the gamma_i variable.
  //gamma_i = 1.0,   for all active topics (i.e., topics with weight > 0)
  //                 topics 0,1 are always active
  //gamma_i = gamma, for the first "new" topic
  //gamma_i = 0.0 ,  otherwise
  void update_gamma(){
    lock_guard<mutex> lock(gamma_update_mutex);  

    bool first=true;
    gamma_i[0]=1.0;    
    gamma_i[1]=1.0;    
    for(int k=2;k<K; ++k){
      if(weight_Z[k] ==0){
        if(first){
          gamma_i[k]=gamma;
          new_k=k;
          first=false;
        }
        else gamma_i[k]=0;
      }
      else gamma_i[k]=1.0;
    }
  }

  //enable automatic topic size growth (Using Chineese Restaurant Process)
  void enable_auto_topics_size(bool v=true){
    fixed_k=!v;
    if(fixed_k){
      for(auto& g : gamma_i){
        g=1.0;
      }
      new_k=-1;
    }
    else{
      update_gamma();
    }
  }

  /// This is where all the magic happens.
  /// Refine the topic labels for Cell c, given the cells in its spatiotemporal neighborhood (nZg), and 
  /// topic model nZW. A gibbs sampler is used to randomly sample new topic label for each word in the 
  /// cell, given the priors.


  //this funciton is for quickly computing topic labels wor the given list of words, without updating the model.
  vector<int> refine_ext( const vector<int>& words,        //list of input words
                          size_t n_iter=10,                //number of refine iterations
                         vector<int> nZg=vector<int>()   //neighborhood prior topic distribution, not including the topics for the given list of words
                        )        
  {
    int K = nZW.size();
    vector<int> topics(words.size());
    if(nZg.empty()) nZg.resize(K,0);
    for(auto& z: topics){
      z = random.category_pdf(gamma_i.begin(), gamma_i.end());
      nZg[z]++;
    }

    vector<float> pz(K,0);
    vector<float> cumulative_pz(K,0);
    for(size_t iter=0;iter<n_iter;++iter)
    for(size_t i=0;i<words.size(); ++i){
      int w = words[i];
      int z = topics[i];
      double total=0;
      nZg[z]--;
      for(size_t k=0;k<K; ++k){
        int nkw = max<int>(0,nZW[k][w]);
        int weight_k = max<int>(0,weight_Z[k]); 
        pz[k] = (nkw+beta)/(weight_k + beta*V) * (nZg[k]+alpha[k]) * mask_Z[k] * gamma_i[k];
        total+=pz[k];
        cumulative_pz[k]=total;
      } 
      //discrete_distribution<> dZ(pz.begin(), pz.end());
      //int z_new = min<int>(dZ(engine),K-1);
      int z_new = random.category_cdf(cumulative_pz.begin(), cumulative_pz.end());
      nZg[z_new]++;
      topics[i]=z_new;
    } 
    return topics;
  }

  vector<int> cooccurence_diffusion(const vector<int>& dist){
    vector<int> out(K);
    for(int k=0;k<K; ++k){
      for(int i=0; i<K; ++i){
        out[i]=int(float(nZZ[k][i])/float(nZZ_weight[k])*dist[k]);
      }
    }
    return out;
  }

  void refine(Cell& c){
    wait_till_unpaused();
    if(c.id >=C)
      return;
    refine_count++;
    vector<int> nZg = neighborhood(c); //topic distribution over the neighborhood (initialize with the cell)
    vector<vector<int>> cell_nZZ;
    if(use_topic_cooccurence){
      nZg=cooccurence_diffusion(nZg);
      cell_nZZ = c.nZZ;
    }

    vector<float> pz(K,0);
    vector<float> cumulative_pz(K,0);

    for(size_t i=0;i<c.W.size(); ++i){
      int w = c.W[i];
      int z = c.Z[i];
      double total=0;
      nZg[z]--;
      for(size_t k=0;k<K; ++k){
        int nkw = max<int>(0,nZW[k][w]-1);
        int weight_k = max<int>(0,weight_Z[k]-1); 
        pz[k] = (nkw+beta)/(weight_k + beta*V) * (nZg[k]+alpha[k]) * mask_Z[k] * gamma_i[k];
        total+=pz[k];
        cumulative_pz[k]=total;
      } 
      //discrete_distribution<> dZ(pz.begin(), pz.end());
      //int z_new = min<int>(dZ(engine),K-1);
      int z_new = random.category_cdf(cumulative_pz.begin(), cumulative_pz.end());

      
      //assert(z_new < K);


      nZg[z_new]++;
      if(update_global_model)relabel(w,z,z_new);
      c.relabel(i,z,z_new);
      if(z_new == new_k) update_gamma(); //new topic created

    } 

    if(use_topic_cooccurence){
      c.update_cooccurence_counts();
      update_cooccurence_counts(cell_nZZ,c.nZZ);
    }
  }

  /// Estimate maximum likelihood topics for the cell
  /// This function is similar to the refine(), except instead of randomly sampling from
  /// the topic distribution for a word, we pick the most likely label.
  /// We do not save this label, but return it. 
  /// the topic labels from this function are useful when we actually need to use the topic
  /// labels for some application.
  vector<int> estimate(Cell& c, bool update_ppx=false){
    //wait_till_unpaused();
    if(c.id >=C)
      return vector<int>();

    vector<int> nZg = neighborhood(c); //topic distribution over the neighborhood (initialize with the cell)
    if(use_topic_cooccurence){
      nZg=cooccurence_diffusion(nZg);
    }

    int weight_c=0;
    double ppx_sum=0;
    if(update_ppx){ 
      c.W_perplexity.resize(c.W.size());
      c.perplexity=0;
      weight_c = accumulate(c.nZ.begin(), c.nZ.end(),0);
    }

    vector<double> pz(K,0);
    vector<int> Zc(c.W.size());

    float alpha_total = accumulate(alpha.begin(),alpha.end(),0.0);
    for(size_t i=0;i<c.W.size(); ++i){
      int w = c.W[i];
      int z = c.Z[i];
      nZg[z]--;
      if(update_ppx) ppx_sum=0;

      for(size_t k=0;k<K; ++k){
        if(k == new_k) 
        {
          pz[k] = 0;
        } 
        else
        {
          int nkw = nZW[k][w];      
          int weight_k = weight_Z[k];
          pz[k] = (nkw+beta)/(weight_k + beta*V) * (nZg[k]+alpha[k]);

          //      if(update_ppx) ppx_sum += pz[k]/(weight_g + alpha*K);
          if(update_ppx){
            ppx_sum += (nkw+beta)/(weight_k + beta*V) * (c.nZ[k]+alpha[k])/(weight_c + alpha_total);
          }
        }
      }

       
      if(update_ppx){
        c.W_perplexity[i]=ppx_sum;
        c.perplexity+=log(ppx_sum);
      }
      Zc[i]= max_element(pz.begin(), pz.end()) - pz.begin();
    }
    //if(update_ppx) c.perplexity=exp(-c.perplexity/c.W.size());

    return Zc;
  }

};



