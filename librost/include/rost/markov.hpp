#ifndef MARKOV_HPP
#define MARKOV_HPP
/*#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/math/distributions.hpp>
#include <boost/random/gamma_distribution.hpp>*/
#include <boost/math/special_functions/digamma.hpp>
#include <vector>
#include <limits>
#include <atomic>
#include <random>
#include <cassert>
//namespace entropy2{
using namespace std;
//using boost::uniform_real;
//using boost::uniform_int;
using boost::math::digamma;

template<typename T>
class Stirling{
public:
  vector<vector<T> > cache;
  Stirling():cache(1,vector<T>(1,T(1))){
  }
  T operator()(unsigned int N, unsigned int m){
    if(N==0 && m==0) return T(1);
    if(m==0 || m > N) return T(0);
    //increase the cache size if needed
    ensure_cache_till(N);
    return cache[N][m];
  }

  void ensure_cache_till(unsigned int N){
    if (N < cache.size())
      return;
    for(size_t ni=cache.size(); ni<=N; ++ni){
      cache.push_back(vector<T>(ni+1));
      cache[ni][0]=T(0);
      cache[ni][ni]=T(1);
      for(size_t mi=1;mi<ni; ++mi){
	cache[ni][mi] = cache[ni-1][mi-1] + (ni-1)*cache[ni-1][mi];
      }
    } 
  }
  T* operator()(unsigned int N){
    //increase the cache size if needed
    ensure_cache_till(N);
    return &(cache[N][0]);
  }
};

class parallel_random{
public:
  vector<mt19937> engine;
  atomic<size_t> next;
  uniform_int_distribution<int> uniform_int;
  parallel_random(int num_threads=16):
    next(0),
    uniform_int(0,numeric_limits<int>::max())
  {
    std::random_device seed_gen; 
    for (int i=0;i<50;++i)
        engine.emplace_back(seed_gen());
  }

  int uniform(int a, int b){
    int k = b-a;
    int r = uniform_int(engine[next++ % engine.size()]);
    return r%k + a;
  }

  template<typename T=double, size_t bits=32>
  T uniform_01(){
    size_t n = next++;
    return generate_canonical<double, bits>(engine[n % engine.size()]);
  }

  template<typename It>
  size_t category_cdf(It begin, It end){
    double u=uniform_01<double,32>() * (*(end -1));
    return lower_bound(begin,end,u) - begin;
  }

  template<typename It>
  size_t category_pdf(It begin, It end){
    vector<typename iterator_traits<It>::value_type > cdf(end-begin);
    std::partial_sum(begin,end,cdf.begin());
    return category_cdf(cdf.begin(),cdf.end());
/*    typename iterator_traits<It>::value_type total=0;
    total = accumulate(begin, end,0);
    size_t k=0;
    double u=uniform_01<double,32>()*total;
    total=0;
    for(It i=begin; i!=end; ++i){
      total+=*i;
      if(u<=total)
        return k;
      k++;
    }
    //cerr<<"ERROR: something wrong with category sampling.\n";
    assert(false);//we should never end up being here
    return k;*/
  }
};

/*
template<typename Float=float,typename Int=int>
class Random{
public:

  typedef boost::variate_generator<boost::mt19937&, boost::uniform_real<Float> > die_01_t;
  typedef boost::variate_generator<boost::mt19937&, boost::uniform_int<Int> > die_t;

  boost::mt19937 randgen;
  uniform_real<Float> uniform_01_dist;
  die_01_t die_01;
  Stirling<double> stirling;


  Random():uniform_01_dist(0,1),
	   die_01(randgen,uniform_01_dist)
  {
  }

  Int die(int N){ //a random number between [0..N-1]
    int r = die_01()*N;
    return r;
  }

  Int integer(int N){ //a random number between [0..N-1]
    int r = die_01()*N;
    assert(r==0 || r<N);
    return r;
  }
    
  template <typename It>
  void uniform_sample(It begin, It end, size_t K){
    die_t die(randgen, uniform_int<Int>(0,static_cast<Int>(K-1)));
    for(; begin!=end; ++begin){
      *begin=die();
    }
  }

  size_t category_sample(const std::vector<float>&p, float total_p){
    Float r = die_01();
    Float u=r*total_p; //normalize it 
    
    total_p=0;
    size_t k;
    for(k=0;k<p.size();k++){
      total_p+=p[k];
      if(u<=total_p) break;
    }
    if(k==p.size()) k--;
     return k;
  }

  template<typename It>
  size_t category_sample_from_pdf(It begin, It end){
    typename iterator_traits<It>::value_type total=0;
    total = accumulate(begin, end,0);
    size_t k=0;
    double u=die_01()*total;
    total=0;
    for(It i=begin; i!=end; ++i){
      total+=*i;
      if(u<=total)
	return k;
      k++;
    }
    cerr<<"ERROR: something wrong with category sampling.\n";
    assert(false);//we should never end up being here
    return k;
  }

  //computes a random sample from the given cdf
  //(end-1) is assumed to be the location storing the total mass
  //uses binary search to speed up things
  template<typename It>
  size_t category_sample_from_cdf(It begin, It end){
    double u=die_01() * (*(end -1));
    return lower_bound(begin,end,u) - begin;
  }

  template<typename T>
  T beta(Float a, Float b){
    return boost::math::cdf(boost::math::beta_distribution<T>(a,b), die_01());
  }

  
  template<typename T>
  T gamma(T shape){
    boost::gamma_distribution<T> gamma_dist(shape);
    boost::variate_generator<boost::mt19937&,boost::gamma_distribution<T> > gamma_gen(randgen, gamma_dist );
    return gamma_gen();

    //return boost::math::cdf(boost::math::gamma_distribution<T>(a), die_01());
  }

  /// generate a sample from a DirichletDistribtion(alpha)
  /// alpha_begin, alpha_end are the iterators for alpha vector
  /// output is written to out iterator. 
  template<typename InIt, typename OutIt>
  OutIt dirichlet(InIt alpha_begin, InIt alpha_end, OutIt out){
    vector<typename iterator_traits<InIt>::value_type> samples(alpha_end - alpha_begin);
    typename vector<typename iterator_traits<InIt>::value_type>::iterator si, si_end;
    si = samples.begin(); si_end=samples.end();

    typename iterator_traits<InIt>::value_type total(0), v;
    for(InIt i=alpha_begin; i!=alpha_end; ++i){
      v=gamma<typename iterator_traits<InIt>::value_type >(*i);
      *si++ = v;
      total+=v;
    }
    for(si=samples.begin(); si!=si_end; ++si){
      *out++ = *si/total;
    }
    return out;
  }

  /// sample the number of components that a DirichletProcess(alpha) has after n samples. 
  /// P(m | alpha, n)  = stirling(n,m) alpha^m Gamma(alpha)/Gamma(alpha+n)
  /// (Antoniak 1974)
  /// we first compute the above distribution, ignoring the gamma constant and then extract a sample
  unsigned int antoniak_sample(double alpha, unsigned int n){
    assert(n>0);
    double * s = stirling(n);
    vector<double> cdf_m(n+1);
    double alpha_to_m=alpha; //alpha^1 = alpha
    cdf_m[0]=0;
    for(size_t m=1;m<=n; ++m){
      cdf_m[m]=cdf_m[m-1]+s[m]*alpha_to_m;
      alpha_to_m *= alpha;
    }
    return category_sample_from_cdf(cdf_m.begin(), cdf_m.end());
  }
};
*/
template<typename T>
vector<int> histogram(const vector<T>& values, unsigned int K=0){
  if(K==0) K = (*(max_element(values.begin(), values.end()))) + 1;
  
  vector<int> hist(K,0);
  if(values.empty())
    return hist;
  
  
  //  hist.resize(K,0);
  for(size_t i=0;i<values.size();++i){
    //if(values[i]>= K)
    //  cerr<<"values[i]="<<values[i]<<"  K="<<K<<endl;
    assert(values[i]<K);
    hist[values[i]]++;
  }
  return hist;
}

template<typename T>
vector<float> normalize(const vector<T>& histogram, float alpha=0.0){
  vector<float> nhist(histogram.size());
  float total=0;
  for(size_t i=0; i<histogram.size(); ++i){
    total+=(histogram[i]+alpha);
  }
  for(size_t i=0; i<histogram.size(); ++i){
    nhist[i]=(static_cast<float>(histogram[i])+alpha)/total;
  }
  return nhist;
}

template<typename T>
double entropy(const vector<T>& histogram){
  T total=0;
  for(size_t i=0; i<histogram.size(); ++i){
    total+=histogram[i];
  }
  double h=0, pi;
  for(size_t i=0; i<histogram.size(); ++i){
    pi=static_cast<double>(histogram[i])/static_cast<double>(total);
    h-=pi*log(pi);
  }
  return h;
}
//returns a histogram of frequency counts of discrete values in the given vector
//K+1 is the number of bins in the returned histogram
//if not specified, K is the maximum value in the the given vlues vector
/*vector<int> histogram(const vector<int>& values, int K=0);

void histogram(const std::vector<int>&values, std::vector<int>& bins);
//void histogram(const cv::Mat_<float>& values, cv::Mat_<float>& bins);

vector<float> normalize(const vector<int>& histogram,float alpha=0);
float entropy(const vector<int>& histogram);
float entropy(const vector<float>& normalized_histogram);
*/

template<typename T>
vector<T> flatten(const vector<vector<T>>& v){
  vector<T> flat;
  for(auto& row: v){
    flat.insert(flat.end(), row.begin(), row.end());
  }
  return flat;
}

template<typename T>
T stddev(const vector<T>& v){
  T sum = accumulate(v.begin(), v.end(), 0.0);
  T mean = sum/v.size();
  T stddev=0;
  for(const T& val: v){
    stddev+=pow(val-mean,2);
  }
  stddev = sqrt(stddev/v.size());
  return stddev; 
}

/// computes trigamma function
//  Reference:
//    BE Schneider, Algorithm AS 121:
//    Trigamma Function,
//    Applied Statistics,
//    Volume 27, Number 1, pages 97-99, 1978.
double trigamma ( double x )
{
  using namespace std;
  double a = 0.0001;
  double b = 5.0;
  double b2 =  0.1666666667;
  double b4 = -0.03333333333;
  double b6 =  0.02380952381;
  double b8 = -0.03333333333;
  double value;
  double y;
  double z;

  assert(x>0.0);

  z = x;
  //
  //  Use small value approximation if X <= A.
  //
  if ( x <= a )
  {
    value = 1.0 / x / x;
    return value;
  }
  //
  //  Increase argument to ( X + I ) >= B.
  //
  value = 0.0;

  while ( z < b )
  {
    value = value + 1.0 / z / z;
    z = z + 1.0;
  }
  //
  //  Apply asymptotic formula if argument is B or greater.
  //
  y = 1.0 / z / z;

  value = value + 0.5 *
  y + ( 1.0
  + y * ( b2
  + y * ( b4
  + y * ( b6
  + y *   b8 )))) / z;

  return value;
}


///inverse of digamma function
///computed using newton's method to find the root of:
///  digamma(x) - y = 0
/// See "Estimating a Dirichlet Distribution", Minka, 2000
double digamma_inverse(double y){
  double x; // digamma_inverse(y)

  //initial values for x
  if(y>=-2.22)
    x=exp(y)+0.5;
  else
    x=-1.0/(y + -digamma(1.0));

  //five iterations give us fourteen digits of pricision
  for(int i=0;i<5;i++){ 
    x = x - (digamma(x) - y)/trigamma(x) ;
  }
  return x;
}

//multinomail_sample is of size [N][K], N multinomials of dim K.
//init_alpha is the dirichlet alpha parameter, of dim K
vector<float> ml_estimate_dirichlet_parameters(const vector<vector<int>>& multinomial_samples, vector<float> alpha){
  unsigned int N=multinomial_samples.size();
  unsigned int K=multinomial_samples[0].size();
  vector<int> total(N,0);
  auto t_it=total.begin();
  for(auto& sample: multinomial_samples){
    *(t_it++) = accumulate(sample.begin(),sample.end(),0);
  }
  vector<float> mean_log_p_k(K);
  vector<float> d_F_d_alpha(K);

  float total_alpha=accumulate(alpha.begin(), alpha.end(),0);

  for(size_t k=0; k< K; ++k){
    float v = 0;
    for(size_t i=0; i< N; ++i){
      v+= log(multinomial_samples[i][k]/total[i]);
    }
    mean_log_p_k[k]=v/float(N);
  }

  for(int i=0; i< 5; ++i){ //5 iterations
    double digamma_alpha_total = digamma(accumulate(alpha.begin(), alpha.end(),0));
    for(size_t k=0; k< K; ++k){
      alpha[k] = digamma_inverse(digamma_alpha_total + mean_log_p_k[k]);
    }
  }
  return alpha;
}

#endif
