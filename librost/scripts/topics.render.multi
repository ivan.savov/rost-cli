#!/usr/bin/env python
import csv
import os
import subprocess
from optparse import OptionParser
import time,datetime


parser = OptionParser()
parser.add_option("-i", "--histlist",
                  help="file containing list of histogram filenames", metavar="FILENAME")
parser.add_option("-v", "--videolist",
                  help="file containint list of  video filenames", metavar="FILENAME")
parser.add_option("-o", "--out-prefix", dest="out_prefix", default="topic",
                  help="output video prefix. each video file name is PEFIX<i>.<ext>, where <i> is the topic number, and ext is same as input video file's extension [default: %default]", metavar="PREFIX")
parser.add_option("-w", "--window", type="int", default=2000,
                  help="radius in milliseconds for each clip, around the selected timestamp [default: %default]")
parser.add_option("-n", "--inhibition", type="int", default=10000,
                  help="after selecting a timestamp, do not select another one within this time radius [default: %default]")
parser.add_option("-m", "--maxclips", type="int", default=20,
                  help="maximum number of clips which will be used to render the topic summary [default: %default]")
parser.add_option("-r", "--normalize-weight", action="store_true", default=False, 
		  help="weight of a topic in at each timestep is normalized by total number of words in that timestep")

(options, args) = parser.parse_args()
if(options.histlist==None or options.videolist==None):
	print "ERROR: must specify topic histogram file and video file"
	exit(1)

# sorted_timestamps is a list of (fileid, timestamp, weight)
# inhibition_radius is the radius around a selected clip, from which we are not allowed to select another clip
# maxclips is the maximum number of clips in the summary
def get_topic_summary(sorted_timestamps, inhibition_radius, maxclips):
	sum_timestamps=[(sorted_timestamps[0][0],sorted_timestamps[0][1])]
	threshold=(sorted_timestamps[0][2])/2
	sorted_timestamps.pop(0)

	for (f,t,w) in sorted_timestamps:
		if w < threshold:
			break
		select_clip=True
		for (sf,st) in sum_timestamps:
			if sf == f and abs(st-t) < inhibition_radius:
				select_clip=False
				break
		if select_clip:
			sum_timestamps.append((f,t))
		if len(sum_timestamps) == maxclips:
			break

	return sum_timestamps

def ms_to_hms(time_in_ms):
	t =[0,0,0]
	if time_in_ms >= 3600000:
		t[0] = time_in_ms / 3600000
		time_in_ms = time_in_ms % 3600000
	if time_in_ms >= 60000:
		t[1] = time_in_ms / 60000
		time_in_ms = time_in_ms % 60000
	t[2]=time_in_ms/1000.0
	return t


def get_ffmpeg_cmd(start_time_ms, duration_ms, in_file, out_file):
	shms = ms_to_hms(start_time_ms)
	dhms = ms_to_hms(duration_ms) 
	cmd = ["ffmpeg", "-ss", "%02d:%02d:%f"%(shms[0],shms[1],shms[2]), "-t", "%02d:%02d:%f"%(dhms[0],dhms[1],dhms[2]), "-i", in_file, "-vcodec", "copy", "-acodec","copy",out_file] 
	'''
	cmd = ["ffmpeg", 
	       "-ss", "%02d:%02d:%f"%(shms[0],shms[1],shms[2]), 
	       "-i", in_file, 
	       #"-ss", "%02d:%02d:%f"%(0,0,3), 
	       "-t", "%02d:%02d:%f"%(dhms[0],dhms[1],dhms[2]), 
	       "-vcodec", "libx264",
	       "-strict", "-2",
	       "-vf", "scale=640:trunc(ow/a/2)*2",
	       "-b:v", "500k",
	       "-b:a", "96k",
	       "-movflags", "+faststart",
	       out_file] 
	       '''
	return cmd


def render_topic_summary(topicnum, summary, window, video_filenames, out_basename):
	timestamp=datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d-%H-%M-%S')
	tmp_concat_file = "tmp_topic_render_concat_%d.txt" %(topicnum)	
	tmpfiles=[tmp_concat_file]
	concat_file = open(tmp_concat_file, 'w')

	print "Extracting videos"

	for i in range(0,len(summary)):
		start_time_ms = max(0,summary[i][1]-window)#min(0,summary[i]- options.window)
		in_video_filename = video_filenames[summary[i][0]]
		vfilepath, vfileextension = os.path.splitext(in_video_filename)
		
		tmp_ofile = "tmp_topic_render_%d_%d_%d_%d.mp4" %(topicnum, i, summary[i][0], summary[i][1])
		tmpfiles.append(tmp_ofile)
		concat_file.write("file '"+tmp_ofile+"'\n")
		concat_file.write("duration "+ str(2*window/1000.0)+"\n")
		cmd=get_ffmpeg_cmd(start_time_ms,2*window,in_video_filename,tmp_ofile)
		print ' '.join(cmd)
		if subprocess.call(cmd) != 0:
			print "error occured while extracting videos"
			map(os.remove, tmpfiles)
			exit(1)
	concat_file.close()

	print "Merging videos"
	oname=out_basename+".mp4"

	cmd = ["ffmpeg",
	       "-f","concat",
	       "-i",tmp_concat_file,
#	       "-c","copy",
	       "-vcodec", "libx264", "-strict", "-2", "-b:v", "500k", "-b:a", "64k", "-g", "1",
	       "-movflags", "+faststart",
	       oname]
	print cmd
	subprocess.call(cmd)
	print "cleaning up"
	#map(os.remove, tmpfiles)



def read_list(filename):
	l=[]
	with open(filename, 'rb') as f:
		reader = csv.reader(f)
		for row in reader:
			if row[0][0] == '#':
				None
			else:
				l.extend(row)

	return l


print "Opening filelist: ",options.histlist
hist_filenames=read_list(options.histlist)
print "Opening video file list: ",options.videolist
video_filenames=read_list(options.videolist)
print video_filenames




#read the topic histograms, and compute number of topics K

K=0
file_topicdist=[]


# data=[
#   [fileid, timestamp, bincount1, bincount2, .... bincount(K-1) ],
#   ...
# ]
data=[] 
fid=0
for filename in hist_filenames:
	print "Reading hist file: ",filename
#	rows=[]
	with open(filename, 'rb') as f:
		reader = csv.reader(f)
		for row in reader:
			if row[0][0] == '#':
				None
			else:
				data.append([fid])
				data[-1].extend(map(int,row))
				K = max(K, len(row)-1)
		fid = fid+1
#print data
#exit(0)
window = options.window

print "window =",window
print "K =",K
print "temporal inhibition = ",options.inhibition

#for each topic, get a list of timestamps, sorted by topic weight
sorted_topics=[] #[ (fileid, timestamp, weight), .. , () ]
for k in range(0,K):
	s=[]
	if options.normalize_weight:
		s = sorted(data, key=lambda row: float(row[k+2])/sum(row[2:]), reverse=True)
	else:
		s = sorted(data, key=lambda row: row[k+2], reverse=True)
	sorted_topics.append(map(lambda row: (row[0],row[1],row[k+1]), s))


#for each topic, select timestamps represetning that topic
topic_summary=[]
#timestamps_out = csv.writer(open(options.out_prefix+"_summary_timestamps.csv", "w"))
for k in range(0,K):
	sum_timestamps = get_topic_summary(sorted_topics[k], options.inhibition, options.maxclips)
	topic_summary.append(sum_timestamps)
#	timestamps_out.writerow(sum_timestamps)
	render_topic_summary(k, sum_timestamps,window,video_filenames ,options.out_prefix+str(k))




