#include<rost/markov.hpp>
#include <iostream>
using namespace std;
int main(){
	
	parallel_random r;
	vector<float> cdf(10,0.0);
	vector<float> pdf(cdf.size(),0.0);
	for(int i=0;i<cdf.size(); ++i){
		pdf[i]= exp(-pow(i-cdf.size()/2.0,2)/4.0);
		cerr<<pdf[i]<<endl;
	}
	cdf[0]=pdf[0];
	for(size_t i=1;i<cdf.size(); ++i){
		cdf[i]=cdf[i-1]+pdf[i]; 
	}

	vector<int> hist(cdf.size(),0);
	int total=0;
	for(int i=0;i<1000000; ++i){
		size_t sample=r.category_cdf(cdf.begin(),cdf.end());
		assert(sample < hist.size());
		hist[sample]++;
		total++;
	}

	for(int i=0; i<hist.size(); ++i){
		int barlen=hist[i]/static_cast<float>(total)*50;
		int j=0;
		for(j=0;j<barlen; ++j){
			cout<<"#";
		}
		for(;j<50; ++j){
			cout<<"-";
		}
		cout<<"("<<hist[i]<<")"<<endl;
	}
}