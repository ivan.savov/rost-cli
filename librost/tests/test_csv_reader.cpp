#include "rost/csv_reader.hpp"
#include "rost/word_reader.hpp"
#include <iterator>
using namespace std;

int main(int argc, char*argv[]){
  vector<vector<int>> data;
  //std::ios_base::sync_with_stdio (false);
  /*{
    csv_reader r(argv[1]);
    while(true){
      auto row = r.next_row();
      if(row.empty()) break;
      vector<int> v= row.get_vector_int();
      //  copy(v.begin(), v.end(), ostream_iterator<int>(cout,"|"));cout<<endl;
      data.push_back(v);
    }
    }*/

  
  {
      word_reader w(argv[1]);
      while(true){
	vector<int> v= w.get();
	if(v.empty()) break;
	copy(v.begin(), v.end(), ostream_iterator<int>(cout,"|")); cout<<endl;
	data.push_back(v);
	
	
      }
  }
  return 0;
}
