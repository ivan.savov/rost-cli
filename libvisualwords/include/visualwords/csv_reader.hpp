#ifndef ROST_CSV_READER_HPP
#define ROST_CSV_READER_HPP
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
using namespace std;
/*
  Word I/O
*/

struct csv_reader{
  istream* stream;
  string line;  
  size_t doc_size;
  char delim;
  vector<int>words;
  csv_reader(string filename, char delim_=','):
    delim(delim_)
  {
    if(filename=="-" || filename == "/dev/stdin"){
      stream  = &std::cin;
    }
    else{
      stream = new ifstream(filename.c_str());
    }
  }  
  /*  vector<int> get(){
    vector<int> words;    
    getline(*stream,line);
    if(*stream){
      stringstream ss(line);
      copy(istream_iterator<int>(ss), istream_iterator<int>(), back_inserter(words));
    }
    return words;
    }*/
  vector<int> get(){
    words.clear();
    vector<string> words_str;
    string word;
    getline(*stream,line);
    if(*stream){
      //cerr<<"Read line: "<<line<<endl;
      stringstream ss(line);      
      while(std::getline(ss,word,delim)){
        words_str.push_back(word);
      }
      transform(words_str.begin(), words_str.end(), back_inserter(words), [](const string& s){return atoi(s.c_str());});
    }
    return words;
  }

  vector<float> get_floats(){
    vector<float>fwords;

    vector<string> words_str;
    string word;
    getline(*stream,line);
    delim=','; //i don't know why delim gets reset sometimes!!!!:(((
    if(*stream){
      //cerr<<"Read line: "<<line<<endl;
      stringstream ss(line);      
      while(std::getline(ss,word,delim)){
        words_str.push_back(word);
      }
      //cerr<<"# elements:"<<words_str.size()<<" delim:"<<(char)delim<<endl;
      transform(words_str.begin(), words_str.end(), back_inserter(fwords), [](const string& s){return atof(s.c_str());});
    }
    return fwords;
  }

  vector<int> peek(){
    return words;
  }
  ~csv_reader(){
    if(stream != &std::cin && stream !=NULL){
      //delete stream;
      stream=0;
    }
  }
};



#endif
