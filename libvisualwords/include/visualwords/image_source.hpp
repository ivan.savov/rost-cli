#ifndef IMAGE_SOURCE_HPP
#define IMAGE_SOURCE_HPP
#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <thread>
#include <condition_variable>

  class ImagePipe
  {
  public:
    //grab moves the pipe forward
    virtual bool grab();
    //retrieve actually does the work if needed and retrieves data
    virtual bool retrieve(cv::Mat& image);
    //set the data source of this node
    //    void set_source(ImagePipe&);
    ImagePipe* source;
    std::string name;
    int id;
    static int last_id;
    ImagePipe(){}
    ImagePipe(std::string name_):name(name_){}
  };
  ImagePipe& operator>(ImagePipe&source, ImagePipe& sink);


  class ImageSource: public ImagePipe
  {
  public:
    //    ImageSource():ImagePipe("ImageSource"){}
    static ImageSource* camera(int device, int width=0, int height=0);
    /// reads a video file
    static ImageSource* videofile(const std::string& filename);
    /// reads an image file
    static ImageSource* imagefile(const std::string& filename);
    /// reads from a text file containing list of filenames
    static ImageSource* imagefilelist(const std::string& filename);
    virtual bool grab()=0;
    virtual bool retrieve(cv::Mat& image)=0;
    virtual ~ImageSource(){}
  };

  class MJPGImageSource:public ImageSource{
  public:
    MJPGImageSource(std::string hostname, std::string port, std::string path);
    bool grab();
    bool retrieve(cv::Mat& image);
  protected:
    void spin(std::string, std::string, std::string);
    std::thread network_thread;
    std::vector<unsigned char> data;
    int img_begin, img_end;
    std::condition_variable grab_cv ;
    std::mutex grab_mutex, data_mutex;
    bool have_data;
  };

  class CVImageSource:public ImageSource{
  public:
    CVImageSource(int cameraid, int width=0, int height=0);
    CVImageSource(const std::string&);
    virtual bool grab();
    virtual bool retrieve(cv::Mat& image);
    virtual double pos_msec();  //Current position of the video file in milliseconds or video capture timestamp.
    virtual double duration_msec();  //Current position of the video file in milliseconds or video capture timestamp.
    virtual unsigned int pos_frame(); //0-based index of the frame to be decoded/captured next.
    void reset();
  protected:
    cv::VideoCapture cap;
    enum source_type_t {CameraSource, FileSource};
    source_type_t source_type;
    int last_grab_frame;
  };

  /// reads an image file
  class ImageFilenameSource:public ImageSource{
  public:
    ImageFilenameSource(const std::string& filename);
    virtual bool grab();
    virtual bool retrieve(cv::Mat& image);
  protected:
    int count;
    bool is_multi;
    std::string filename;
  };

  /// reads a text file containing image filenames
  class ImageFilenameListSource:public ImageSource{
  public:
    ImageFilenameListSource(const std::string& filename);
    virtual bool grab();
    virtual bool retrieve(cv::Mat& image);
  protected:
    bool done;
    int count;
    std::string filename;
    std::ifstream in_filelist;    
  };

  class Subsample: public ImagePipe{
  public:
    Subsample(int rate);
    bool grab();
  protected:
    int rate;
  };

  class Scale: public ImagePipe{
  public:
    Scale(double scale);
    bool retrieve(cv::Mat& image);
  protected:
    double scale;
  };


#endif
