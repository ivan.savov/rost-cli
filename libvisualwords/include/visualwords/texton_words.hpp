#ifndef ROST_TEXTON_BOW_HPP
#define ROST_TEXTON_BOW_HPP
#include "bow.hpp"
//#include "flann_matcher.hpp"
#include<opencv2/flann/flann.hpp>

//using namespace cv;

struct TextonBOW:public BOW{
  int size_cols;
  double img_scale;
  int vocab0;
  std::vector<double> thetas, lambdas;
  std::vector<cv::Mat> sin_gabor, cos_gabor;
//std::vector<cv::Mat_<float> >filter_response;
//  cv::Ptr<FlannMatcher> flann_matcher;
//  cv::Ptr<cv::flann::GenericIndex<cv::flann::L2<float> > >flann_matcher;
  cv::flann::GenericIndex<cv::flann::L2<float> > * flann_matcher;
  cv::Mat vocabulary;

  TextonBOW(int vocabulary_begin_=0, int size_cols_=64, double img_scale_=1.0, const std::string& vocab="");

  std::vector<cv::Mat> apply_filter_bank(cv::Mat& img);
  std::vector<std::vector<float> > compute_feature_vector(std::vector<cv::Mat>& response);
  cv::Mat compute_feature_mat(std::vector<cv::Mat>& response);
  std::vector<unsigned int> make_words(std::vector<cv::Mat>& feature_vec);

  void load_vocabulary(const std::string& filename);

  WordObservation operator()(cv::Mat& img);
};

#endif
