#ifndef ROST_GABOR_BOW_HPP
#define ROST_GABOR_BOW_HPP
#include "bow.hpp"


//using namespace cv;

struct GaborBOW:public BOW{
  int size_cols;
  double img_scale;
  int vocab0;
std::vector<double> thetas, lambdas;
std::vector<cv::Mat> sin_gabor, cos_gabor;
std::vector<cv::Mat_<float> >filter_response;

  GaborBOW(int vocabulary_begin_=0, int size_cols_=64, double img_scale_=1.0);

  std::vector<unsigned int> make_words(std::vector<cv::Mat_<float> >& response);

  WordObservation operator()(cv::Mat& imgs);
};

#endif
