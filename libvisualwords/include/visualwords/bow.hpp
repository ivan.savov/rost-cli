#pragma once
#ifndef ROST_BOW_HPP
#define ROST_BOW_HPP
#include <opencv2/opencv.hpp>
#include <vector>
#include <string>


//namespace rost{  

  struct WordObservation{
    unsigned int seq;  
    std::string source; //name of the word source
    std::vector<int>  observation_pose, //pose of this observation
      words,      //list of words in this observation
      word_pose,  //pose of each word in this cell
      word_scale; //size of words in this observation

    //words are in the range [vocabulary_begin, vocabulary_begin + vocabulary_size)
    int vocabulary_begin, vocabulary_size;
    WordObservation():vocabulary_size(0),vocabulary_begin(0){}
  };

  struct BOW{    
    std::string name;
    int vocabulary_begin;
    int vocabulary_size;
    bool visualize; //enable visualization
    BOW(const std::string& name_, int vb, int vs=0):name(name_), vocabulary_begin(vb), vocabulary_size(vs),visualize(false){}
    virtual WordObservation operator()(cv::Mat& img)=0;   
    void enable_visualization(bool v){visualize=v;}
  };

  struct MultiBOW: public BOW{
    std::vector<BOW*> bow;
    MultiBOW():BOW("Multi:",0){

    }
    void add(BOW * b){
      b->vocabulary_begin=vocabulary_size;
      vocabulary_size+=b->vocabulary_size;
      name+=std::string(" ")+b->name;
      bow.push_back(b);
    }
    WordObservation operator()(cv::Mat& img){
      WordObservation z;
      z.vocabulary_size=vocabulary_size;
      z.vocabulary_begin=vocabulary_begin;
      for(size_t i=0;i<bow.size(); ++i){
        WordObservation zi = (*(bow[i]))(img);
        z.words.insert(z.words.end(), zi.words.begin(), zi.words.end());
        z.word_pose.insert(z.word_pose.end(), zi.word_pose.begin(), zi.word_pose.end());        
        z.word_scale.insert(z.word_scale.end(), zi.word_scale.begin(), zi.word_scale.end());        
      }
      return z;
    }

  };
//}

  /// removes the words which are not covered by the mask
  WordObservation apply_mask(const WordObservation& z, const cv::Mat& mask);
#endif
