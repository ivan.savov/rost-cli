#include "feature_detector.hpp"
#include "opencv2/opencv.hpp"
#include "bow.hpp"

class FlannMatcher;
struct FeatureBOW:public BOW{
    //    cv::Ptr<cv::FeatureDetector> feature_detector;
  vector<cv::Ptr<cv::FeatureDetector> >feature_detectors;
  vector<string >feature_detector_names;
  string feature_descriptor_name;
  cv::Ptr<cv::DescriptorExtractor> desc_extractor;
  //    cv::Ptr<cv::DescriptorMatcher> desc_matcher;
  cv::Ptr<cv::DescriptorMatcher>  desc_matcher;
  cv::Ptr<FlannMatcher> flann_matcher;
  cv::Mat vocabulary;
  
  double img_scale;
  
  
  FeatureBOW(int vocabulary_begin_, 
	     const string& vocabulary_filename, 
	     const vector<string>& feature_detector_names_, 
	     const vector<int>& feature_sizes_, 
	     const string& feature_descriptor_name_, 
	     double img_scale_=1.0);
  
  WordObservation operator()(cv::Mat& img);
};


struct LabFeatureBOW: public BOW{
    std::vector<BOW*> bow;
    LabFeatureBOW(int vocabulary_begin_, 
       const string& vocabulary_filename, 
       const vector<string>& feature_detector_names_, 
       const vector<int>& feature_sizes_, 
       const string& feature_descriptor_name_,
       double img_scale_=1.0);

    WordObservation operator()(cv::Mat& img);

  };
