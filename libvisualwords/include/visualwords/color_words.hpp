#ifndef ROST_COLORWORDS
#define ROST_COLORWORDS

#include <vector>
#include <opencv/cv.h>
#include "bow.hpp"
using namespace std;
  struct ColorBOW:public BOW{
    int size_cols;
    double img_scale;
    bool use_hue, use_intensity;
    int hvocab0, ivocab0;
    ColorBOW(int vocabulary_begin_=0, int size_cols_=32, double img_scale_=1.0, bool use_hue_=true, bool use_intensity_=true);
    WordObservation operator()(cv::Mat& imgs);
  };

#endif