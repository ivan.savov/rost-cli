//#include "epitomizer2.hpp"
//#include "descriptor.hpp"
//#include "densesurf.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/features2d/features2d.hpp"

#include <visualwords/image_source.hpp>
#include <visualwords/texton_words.hpp>
#include <iostream>
#include <fstream>
#include <boost/program_options.hpp>


using namespace std;
namespace po = boost::program_options;
using namespace cv;

template<typename T>
void write_table(ostream& out, cv::Mat& m){
  for(int i=0;i<m.rows;++i){
    cv::Mat row = m.row(i);
    copy(row.begin<T>(), row.end<T>()-1, ostream_iterator<T>(out,","));
    out<<*(row.end<T>()-1);
    out<<endl;
  }
}
template<>
void write_table<unsigned char>(ostream& out, cv::Mat& m){
  for(int i=0;i<m.rows;++i){
    cv::Mat row = m.row(i);
    copy(row.begin<unsigned char>(), row.end<unsigned char>(), ostream_iterator<int>(out," "));
    out<<endl;
  }
}

///process program options..
///output is variables_map args
int read_program_options(int argc, char*argv[], po::variables_map& args){
  po::options_description desc("Trains a Texton vocabulary");
  desc.add_options()
    ("help", "help")
    ("video", po::value<string>(),"Video file")
    ("camera", "Use Camera")
    ("image", po::value<string>(),"Image file")
    ("image-list", po::value<string>(), "Text file containing list of image filenames")
    ("subsample", po::value<int>()->default_value(1),"Subsampling rate for input sequence of images and video.")
    //("fps",po::value<double>()->default_value(-1),"fps for input")
    ("numframe,N", po::value<int>()->default_value(0)," Number of input images (0=no limit)")
    ("scale",po::value<float>()->default_value(1.0),"Scale image")
    ("vocabsize,K",po::value<int>()->default_value(1000), "Vocabulary size.(Only needed for training task)")

    ("vocabulary,v", po::value<string>()->default_value("texton.vocabulary.csv"),"Vocabulary file. In training this specifies the outfile, whereas in description mode, it specifies the input vocabulary")
    ;

  po::positional_options_description pos_desc;
  pos_desc.add("task", -1);
  

  po::store(po::command_line_parser(argc, argv)
	    .style(po::command_line_style::default_style ^ po::command_line_style::allow_guessing)
	    .options(desc)
	    .positional(pos_desc)
	    .run(), 
	    args);
  //po::store(po::command_line_parser(argc, argv).options(desc).run(), args);
  po::notify(args);    

  if (args.count("help")) {
    cerr << desc << "\n";
    exit(0);
  }
  return 0;
}


int main(int argc, char*argv[]){
  po::variables_map ARGS;
  read_program_options(argc,argv, ARGS);



  std::ofstream metaout("textons_train_meta.txt");
  metaout<<"nwords: "<<ARGS["vocabsize"].as<int>()<<endl
	 <<"scale: "<<ARGS["scale"].as<float>()<<endl
	 <<"subsample :"<<ARGS["subsample"].as<int>()<<endl
    //   <<"thresholdsurf: "<<ARGS["thresholdsurf"].as<double>()<<endl
	 <<"vocab: "<<ARGS["vocabulary"].as<string>()<<endl
    ;
  metaout.close();    
  

  ImageSource * img_source;

  if(ARGS.count("video"))
    img_source = ImageSource::videofile(ARGS["video"].as<string>());
  else if(ARGS.count("image"))
    img_source = ImageSource::imagefile(ARGS["image"].as<string>());
  else if(ARGS.count("image-list"))
    img_source = ImageSource::imagefilelist(ARGS["image-list"].as<string>());
  else
    img_source = ImageSource::camera(0);


  cv::Mat img,imgs;


  bool done=false;
  int imgid=0;
  int numframe=0;
  Subsample subsample(ARGS["subsample"].as<int>());
  Scale scale(ARGS["scale"].as<float>());
  //pipeline
  *img_source > subsample > scale;


  //choose and configure the right feature detector



  cv::Mat binary_vocabulary;


  TextonBOW texton(0,128,ARGS["scale"].as<float>());
  //texton.visualize=true;

  vector<vector<float> > all_features;
//  cv::Mat data;

  cerr<<"Starting.."<<endl;
  while(!done && (ARGS["numframe"].as<int>()==0 || numframe < ARGS["numframe"].as<int>())){
    done=!scale.grab();
    
    if(done)
    	{std::cerr<<"Done reading files.\n"; continue;}

    scale.retrieve(imgs);

    //returns #orientations x #scales x 3 color channels (in Lab space) response matrices
    vector<cv::Mat> response=texton.apply_filter_bank(imgs);
    vector<vector<float> > features=texton.compute_feature_vector(response);
    all_features.insert(all_features.end(),features.begin(),features.end());

    imgid+=ARGS["subsample"].as<int>();  
    cerr<<"frame: "<<numframe<<endl;
    numframe++;
    cv::waitKey(1);
  }
  delete img_source;

  cerr<<"Extracted "<<all_features.size()<<" features, of dim "<<all_features[0].size()<<endl;

  cerr<<"Clustering into "<<ARGS["vocabsize"].as<int>()<<endl;
  int clusterCount = ARGS["vocabsize"].as<int>();
  Mat centers(clusterCount,all_features[0].size(),CV_32F);
  Mat labels(all_features.size(),1,CV_32S);
  int attempts = 1;
  cv::Mat data(all_features.size(), all_features[0].size(), CV_32F);
  for(int i=0;i<all_features.size(); ++i)
    for(int j=0;j<all_features[i].size(); ++j){
      data.at<float>(i,j)=all_features[i][j];
    }
  //cv::Mat data(all_features);
  cerr<<"Data: "<<data.rows<<" x "<<data.cols<<endl;
  kmeans(data, clusterCount, labels, TermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS, 10, 0.001), attempts, KMEANS_PP_CENTERS, centers );

  cerr<<"Writing vocabulry to: "<<ARGS["vocabulary"].as<string>()<<endl;
  ofstream vout(ARGS["vocabulary"].as<string>().c_str());
  write_table<float>(vout,centers);
  cerr<<"Done!"<<endl;
  return 0;
}

