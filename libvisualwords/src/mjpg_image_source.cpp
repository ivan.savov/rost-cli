#include <visualwords/image_source.hpp>
#include<iostream>
#include<string>
#include <boost/asio.hpp>
using namespace std;
using boost::asio::ip::tcp;

MJPGImageSource::MJPGImageSource(string hostname, string port, string path)

{
  have_data=false;
  network_thread = std::thread(&MJPGImageSource::spin,this,hostname, port, path);  
}
bool MJPGImageSource::grab(){
  /*if(data.size()<4) return false;
  img_begin=-1;
  img_end=-1;  
  for(size_t i=data.size()-2; i>0; --i){
    if(img_end==-1 && data[i]==0xff && data[i+1]==0xd9){
      img_end=i;
    }
    else if(img_end!=-1 && img_begin==-1 && data[i]==0xff && data[i+1]==0xd8){
      img_begin=i;
      return true;
    }
  }
  return false;*/
  return true;
}

bool MJPGImageSource::retrieve(cv::Mat& img){
  //std::unique_lock<std::mutex> lk(grab_mutex);
  cerr<<"Waiting for data"<<endl;
  while(!have_data){
  //    grab_cv.wait(lk);
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
  cerr<<"Got data"<<endl;
  if(data.size()<4) return false;
  img_begin=-1;
  img_end=-1;  
  lock_guard<mutex> data_lock(data_mutex);
  for(size_t i=data.size()-2; i>0; --i){
    if(img_end==-1 && data[i]==0xff && data[i+1]==0xd9){
      img_end=i;
    }
    else if(img_end!=-1 && img_begin==-1 && data[i]==0xff && data[i+1]==0xd8){
      img_begin=i;
      break;
      //return true;
    }
  }

  if(img_begin!=-1 && img_end !=-1){
    size_t n = img_end - img_begin+2;
    cv::imdecode(cv::Mat(1,n,CV_8UC1,(void*)(&data[img_begin]) ), CV_LOAD_IMAGE_COLOR, &img);
    data.erase(data.begin(), data.begin()+img_end+2);    
    img_begin=-1; img_end=-1;
    have_data=false;
    return true;
  }  
  return false;
}

void MJPGImageSource::spin(string hostname, string port, string path){
  boost::asio::io_service io_service; 
  tcp::resolver resolver(io_service);
  tcp::resolver::query query(hostname, port);
  tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
  tcp::socket socket(io_service);
  boost::system::error_code error;

  std::unique_lock<std::mutex> lk(grab_mutex);
  while(true){
    boost::asio::connect(socket, endpoint_iterator,error);
    if(error){
      cerr<<"Error connecting"<<endl;
      this_thread::sleep_for(chrono::milliseconds(1000));
      continue;
    }
    boost::asio::streambuf request;
    std::ostream rstream(&request);

    rstream << "GET "<<path<<" HTTP/1.0\r\n";
    rstream << "Host: "<<hostname<<"\r\n";
    rstream << "Accept: */*\r\n";
    rstream << "Connection: close\r\n\r\n";

    boost::asio::write(socket, request);

    boost::asio::streambuf response;
    boost::asio::read_until(socket, response, "\r\n");

    // Check that response is OK.
    std::istream response_stream(&response);
    std::string http_version;
    response_stream >> http_version;
    unsigned int status_code;
    response_stream >> status_code;
    std::string status_message;
    std::getline(response_stream, status_message);
    if (!response_stream || http_version.substr(0, 5) != "HTTP/")
      {
	std::cerr << "Invalid response\n";
	continue;
      }
    if (status_code != 200)
      {
	std::cerr << "Response returned with status code " << status_code << "\n";
	continue;
      }
  
    // Read the response headers, which are terminated by a blank line.
    boost::asio::read_until(socket, response, "\r\n\r\n");

    // Process the response headers.
    std::string header;
    while (std::getline(response_stream, header) && header != "\r")
      std::cerr << header << "\n";
    std::cerr << "\n";

    // Write whatever content we already have to output.
    if (response.size() > 0)
      std::cerr << &response;

    // Read until EOF, writing data to output as we go.

    //while (boost::asio::read(socket, response, boost::asio::transfer_at_least(4096), error)){
    while (true){

      int n=boost::asio::read_until(socket, response, "\xff\xd9", error);
      //cerr<<"Read "<<n<<" bytes"<<endl;
      if(error) break;

      {
	lock_guard<mutex> data_lock(data_mutex);
	//keep the size in check
	if(data.size()>1000000){
	  data.erase(data.begin(), data.begin()+data.size()/2);
	}
	data.insert(data.end(), (istreambuf_iterator<char>(&response)), 
		    istreambuf_iterator<char>());
	//cerr<<"Data inserted:"<<data.size()<<endl;
	have_data=true;
      }
      //grab_cv.notify_one();

      response.consume(response.size());
      //consume();
    }
    if (error == boost::asio::error::eof){
      cerr<<"Connection closed"<<endl;
    }
    else{
      cerr<<"Unknown error"<<endl;
      throw boost::system::system_error(error);
    }
    cerr<<"Attempting to reconnect"<<endl;
  
  }

}



