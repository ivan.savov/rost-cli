#include <visualwords/image_source.hpp>

#include<iostream>

using namespace std;
using namespace cv;

  int ImagePipe::last_id(0);

/*  ImagePipe::ImagePipe():
    id(last_id++)
  {    
    name=std::string("Pipe:");
  }
*/
  bool ImagePipe::grab(){
    return source->grab();
  }

  bool ImagePipe::retrieve(cv::Mat&img){
    //    std::cerr<<name<<" retrieve\n";
    return source->retrieve(img);
  }


  ImagePipe& operator>(ImagePipe&source, ImagePipe& sink){
    sink.source = &source; 
    std::cerr<<"Connected "<<source.name<<" >> "<<sink.name<<std::endl;
    return sink;
  }
  
  ImageSource* ImageSource::camera(int device, int w, int h){
    return new CVImageSource(device,w,h);
  }
  ImageSource* ImageSource::videofile(const string& filename){
    return new CVImageSource(filename);
  }
  ImageSource* ImageSource::imagefile(const string& filename){
    return new ImageFilenameSource(filename);
  }
  ImageSource* ImageSource::imagefilelist(const string& filename){
    return new ImageFilenameListSource(filename);
  }


  

CVImageSource::CVImageSource(int device, int w, int h):cap(device){
  name="camera";
  source_type=CameraSource;
  last_grab_frame=-1;
  if(w >0 && h>0){
    cap.set(CV_CAP_PROP_FRAME_WIDTH, (double) w);
    cap.set(CV_CAP_PROP_FRAME_HEIGHT, (double) h);
  }
}
CVImageSource::CVImageSource(const string& filename):cap(filename){
  name="videofile"; 
  source_type=FileSource;
  last_grab_frame=-1;
}
bool CVImageSource::grab(){
  //std::cerr<<"ImageSource::Grab()"<<std::endl;
  bool success=cap.grab();
  if(source_type==FileSource){
    int current_frame_num = pos_frame();
    if(last_grab_frame > current_frame_num){
      cerr<<"Last frame num: "<<last_grab_frame<<"  current frame num: "<<current_frame_num<<endl;
      cerr<<"Warning! video looped to the begining. stopping."<<endl;
      return success = false; // we have looped around
    }
    last_grab_frame = current_frame_num;
  }
  return success;
}
bool CVImageSource::retrieve(cv::Mat& image){
  return cap.retrieve(image);
}

double CVImageSource::pos_msec(){
  return cap.get(CV_CAP_PROP_POS_MSEC);
}
void CVImageSource::reset(){
  cap.set(CV_CAP_PROP_POS_FRAMES,0); 
}
double CVImageSource::duration_msec(){
  double pos_frame = cap.get(CV_CAP_PROP_POS_FRAMES); //save position
  cap.set(CV_CAP_PROP_POS_AVI_RATIO,1.0);
  double duration=cap.get(CV_CAP_PROP_POS_MSEC);
  cap.set(CV_CAP_PROP_POS_FRAMES,pos_frame); //restore position
  return duration;
}
unsigned int CVImageSource::pos_frame(){
  return static_cast<unsigned int>(cap.get(CV_CAP_PROP_POS_FRAMES));
}

/////////////////////////////////////////////////

  

  ImageFilenameSource::ImageFilenameSource(const string& f)
    :count(0), is_multi(false), filename(f)
  {
  }
  bool ImageFilenameSource::grab(){
    if(!is_multi && count>0)
      return false;

    count++;
    return true;
  }
  bool ImageFilenameSource::retrieve(cv::Mat& image){
    image=cv::imread(this->filename);
    return true;
  }


/////////////////////////////////////////////////

  
  ImageFilenameListSource::ImageFilenameListSource(const string& f)
    :done(false), count(0), filename(""), in_filelist(f)
  {
    cerr<<"Initializing file list reader: "<<f<<endl;
  }
  bool ImageFilenameListSource::grab(){
    if(done)
      return false;
    in_filelist >> filename;
    if(!done && !in_filelist){
      done=true;
      return false;
    }
    cerr<<"Grab: "<<filename<<endl;
    count++;
    return true;
  }
  bool ImageFilenameListSource::retrieve(cv::Mat& image){
    image=cv::imread(this->filename);
    return true;
  }


  //////////////////////////////////////////////////////////

Subsample::Subsample(int r):ImagePipe("subsample"),rate(r){}

  bool Subsample::grab(){
    assert(rate>=0);
    if(rate==0)
      return false;
    for(int i=0;i<rate-1;++i){
      //cerr<<"subsampling: "<<i<<endl;
      if(! source->grab()){
	return false;
      }
    }
    bool r=source->grab();
    return r;
  }

Scale::Scale(double s):ImagePipe("scale"),scale(s){}

  bool Scale::retrieve(cv::Mat& img){
    if(source->retrieve(img)){
      if(scale!=1.0){
	cv::Mat imgs;
	cv::resize(img,imgs,cv::Size(),scale,scale);
	img=imgs;
      }
      return true;
    }
    return false;
  }
  

