#include <visualwords/color_words.hpp>
using namespace std;

ColorBOW::ColorBOW(int vocabulary_begin_, int size_cols_, double img_scale_, bool use_hue_, bool use_intensity_):
  BOW("",vocabulary_begin_,0),
  size_cols(size_cols_),
  img_scale(img_scale_),
  use_hue(use_hue_), use_intensity(use_intensity_)
{
  if(use_hue) name="Hue";
  if(use_intensity) name+="Light";

  assert(use_hue || use_intensity);
  if(use_hue){
    cerr<<"Initializing Hue Words"<<endl;
    hvocab0=vocabulary_begin+vocabulary_size;
    BOW::vocabulary_size+=180;
  }
  if(use_intensity){
    cerr<<"Initializing Intensity Words"<<endl;
    ivocab0=vocabulary_begin+vocabulary_size;
    BOW::vocabulary_size+=256;
  }
}

WordObservation ColorBOW::operator()(cv::Mat& imgs){


  cv::Mat thumb;
  int size_rows = size_cols*static_cast<float>(imgs.rows)/imgs.cols;
  cv::resize(imgs,thumb,cv::Size(size_cols,size_rows));
  cv::Mat hsv (size_rows, size_cols, CV_8UC3);
  cv::Mat_<uchar> hue (hsv.rows, hsv.cols);
  cv::Mat_<uchar> saturation (hsv.rows, hsv.cols);
  cv::Mat_<uchar> value (hsv.rows, hsv.cols);
  cv::cvtColor(thumb,hsv,CV_BGR2HLS);
  cv::Mat splitchannels[]={hue,value,saturation};
  cv::split(hsv,splitchannels);


  WordObservation z;
  z.source=name;
  //      z.seq = image_seq;
  //      z.observation_pose=pose;
  z.vocabulary_begin=vocabulary_begin;
  z.vocabulary_size=vocabulary_size;

  //width of each pixel in the original image
  float word_scale = static_cast<float>(imgs.cols)/size_cols/img_scale;

  vector<int> word_pose(2,0);
  for(int i=0;i<thumb.rows; ++i) // y
    for(int j=0;j<thumb.cols; ++j){ //x
      if(use_intensity){
        z.words.push_back(ivocab0 + value(i,j));
        z.word_pose.push_back(j*word_scale + word_scale/2);
        z.word_pose.push_back(i*word_scale + word_scale/2);
        z.word_scale.push_back(word_scale/2);
      }
      if(use_hue){
        z.words.push_back( hvocab0 + hue(i,j)); //hue is from 0..180
        z.word_pose.push_back(j*word_scale + word_scale/2);
        z.word_pose.push_back(i*word_scale + word_scale/2);
        z.word_scale.push_back(word_scale/2);
      }
    }
  //cerr<<"#color-words: "<<z->words.size();
  return z;
}

