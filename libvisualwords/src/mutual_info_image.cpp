#include<iostream>
#include<cassert>
#include<unordered_map>
#include<tuple>
#include<algorithm>
#include<map>
#include "opencv2/opencv.hpp"

#include "rost/io.hpp"
#include "rost/markov.hpp"

using namespace std;

tuple<vector<vector<int>>, size_t> load_topics(const string& csv){
  csv_reader in_words(csv);

  vector<int> row = in_words.get();
  vector<vector<int>> rows;
  int K=0;
  while(!row.empty()){
    rows.emplace_back(row.begin()+1, row.end());
    K = max((int)K, *max_element(row.begin()+1, row.end()));
    row = in_words.get();    
  }
  return make_tuple(rows,K+1);
}

tuple<vector<vector<int>>, size_t> load_topics_img(const string& filename){
  cv::Mat_<cv::Vec3b>  image=cv::imread(filename);
  cerr<<"Loaded "<<image.rows<<"x"<<image.cols<<" image"<<endl;
/*  cv::Mat hls (image.rows, image.cols, CV_8UC3);
  cv::Mat_<uchar> hue (hls.rows, hls.cols); 
  cv::Mat_<uchar> lightning (hls.rows, hls.cols);
  cv::Mat_<uchar> saturation (hls.rows, hls.cols);
  cv::cvtColor(image,hls,CV_BGR2HLS);
  cv::Mat splitchannels[]={hue,lightning,saturation};
  cv::split(hls,splitchannels);*/

  vector<vector<int>> rows(image.rows,vector<int>(image.cols,0));
  unordered_map<int,int> indexmap;
  for(size_t i=0; i< image.rows; ++i){
    for(size_t j=0; j< image.cols; ++j){
      int v = image(i,j)[0]+ 256*(image(i,j)[1]+ 256*image(i,j)[2]);
      auto it = indexmap.find(v);
      int vv;
      if(it==indexmap.end()){
        vv=indexmap.size();
        indexmap[v]=vv;
        //cerr<<vv<<" -> "<< (int)image(i,j)[0]<<","<<(int)image(i,j)[1]<<","<<(int)image(i,j)[2]<<"  @"<<i<<","<<j<<endl;
      }
      else{
        vv = it->second;
      }
      rows[i][j]=vv;
    }
    //    cerr<<i<<"-"<<rows[i][0]<<endl;    
  }
  return make_tuple(rows,indexmap.size());
}

tuple<vector<vector<int>>, int> compute_joint(vector<vector<int>>& v1, size_t S1, vector<vector<int>>& v2, size_t S2){
  vector<vector<int>> joint(S1, vector<int>(S2,1));
  int total(S1*S2);
  vector<int> v1f = flatten(v1), v2f = flatten(v2);

  assert(v1f.size() == v2f.size());
  for(size_t t=0; t<v1f.size(); ++t){
    int b1 = v1f[t];
    int b2 = v2f[t];
    joint[b1][b2]++; total++;
  }
  return make_tuple(joint,total);
}


tuple<vector<int>, int> compute_count(vector<vector<int>>& v, size_t S){
  vector<int> count(S,1);
  int total(S);
  for(auto a: v) for(auto b:a ){ count[b]++; total++;}
  return make_tuple(count,total);
}

void out(vector<vector<int>>& topics){
  for(auto l: topics){
    for(auto l2: l)
      cout<<l2<<" ";
    cout<<endl;
  }
}
void out(vector<int>& topics){
  for(auto l: topics){
      cout<<l<<" ";
    cout<<endl;
  }
}
int main(int argc, char* argv[]){

  string topic1_csv_filename=argv[1];
  string topic2_csv_filename=argv[2];

  vector<vector<int>> topics,labels;

//  csv_reader in1(topic1_csv_filename);
//  csv_reader in2(topic2_csv_filename);

  size_t L,K;
  cerr<<"Reading first file: "<<topic1_csv_filename<<endl;
  tie(topics,K) = load_topics_img(topic1_csv_filename);  
  cerr<<"Reading second file: "<<topic2_csv_filename<<endl;
  tie(labels,L) = load_topics_img(topic2_csv_filename);

  cerr<<"# labels in first: "<<K<<endl
      <<"# labels in second: " <<L<<endl;

  vector<int> count_topics, count_labels;
  vector<vector<int>> joint;
  int topics_total, labels_total, joint_total;
  cerr<<"Computing counts for 1"<<endl;
  tie(count_topics, topics_total) = compute_count(topics,K);
  cerr<<"Computing counts for 2"<<endl;
  tie(count_labels, labels_total) = compute_count(labels,L);

  cerr<<"Compuring Joint"<<endl;
  tie(joint, joint_total) = compute_joint(labels,L,topics,K);
  //  out(joint);
  //  cerr<<joint_total<<" "<<labels_total<<" "<<topics_total<<endl;
  //  out(count_labels);cout<<endl;
  //  out(count_topics);
  double mi=0;//mutual information
  double h_x, h_y=0; //entroy of individual variables
  for(size_t l=0;l<L; ++l){
    for(size_t k=0; k<K; ++k){
      double p_x_y = static_cast<double>(joint[l][k])/joint_total;
      double p_x = static_cast<double>(count_labels[l])/labels_total;
      double p_y = static_cast<double>(count_topics[k])/topics_total;
      mi+= p_x_y*log2(p_x_y/p_x/p_y); 
    }
  }

  for(size_t l=0;l<L; ++l){
    //cerr<<count_labels[l]<<",";
    double p_x = static_cast<double>(count_labels[l])/labels_total;
    h_x-= p_x*log2(p_x); 
  }
  //cerr<<endl;

  for(size_t k=0; k<K; ++k){
    //cerr<<count_topics[k]<<",";
    double p_y = static_cast<double>(count_topics[k])/topics_total;
    h_y-= p_y*log2(p_y); 
  }
  //cerr<<endl;

  
  cout<<mi<<endl;     
  return 0;
}
