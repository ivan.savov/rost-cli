//
// Created by yogesh on 7/18/15.
//

#include "visualwords/bow.hpp"

/// removes the words which are not covered by the mask
WordObservation apply_mask(const WordObservation& z, const cv::Mat& mask){
    WordObservation zout = z;
    zout.words.clear();
    zout.word_pose.clear();
    zout.word_scale.clear();
    assert(z.word_pose.size() == z.words.size()*2);
    for(size_t i = 0; i< z.words.size(); ++i){
        int w = z.words[i];
        int x = z.word_pose[i*2];
        int y = z.word_pose[i*2+1];
        int s = z.word_scale[i];
        if(mask.at<char>(y,x)){
            zout.words.push_back(w);
            zout.word_pose.push_back(x);
            zout.word_pose.push_back(y);
            zout.word_scale.push_back(s);
        }
    }
    return zout;
}