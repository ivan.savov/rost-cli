#include <visualwords/texton_words.hpp>
#include <rost/csv_reader.hpp>

//namespace rost{
  using namespace std;
//  using namespace rost_common;
  using namespace cv;
  
  TextonBOW::TextonBOW(int vocabulary_begin_, int size_cols_, double img_scale_, const string& vocab):
    BOW("Texton",vocabulary_begin_,0),
    size_cols(size_cols_),
    img_scale(img_scale_),
    flann_matcher(nullptr)
  {
    BOW::vocabulary_size = 1000; 
    thetas.push_back(0.0); thetas.push_back(45.0); thetas.push_back(90.0); thetas.push_back(135.0);
    lambdas.push_back(2.0); lambdas.push_back(4.0); lambdas.push_back(8.0); lambdas.push_back(16.0);//lambdas.push_back(17.0); 
    //    thetas = {{0.0, 45.0, 90.0, 135.0}};
    //    lambdas = {{2.0, 5.0, 11.0, 17.0}};
    size_t n=thetas.size()*lambdas.size();
    sin_gabor.resize(n);
    cos_gabor.resize(n);

    double gamma = 1.0;
    //initialize the filter bank
    int i=0;
    //    for(double theta: thetas){
    //      for(double lambda: lambdas){
    for(size_t ti=0; ti< thetas.size(); ++ti){
      for(size_t li=0; li< lambdas.size(); ++li){
        double theta = thetas[ti]; double lambda = lambdas[li];
        double sigma = lambda*0.5;
        sin_gabor[i] = getGaborKernel( Size(), sigma, theta/180.0*M_PI, lambda,  gamma, M_PI/2.0, CV_32F );
        cos_gabor[i] = getGaborKernel( Size(), sigma, theta/180.0*M_PI, lambda,  gamma, 0.0, CV_32F );
        i++;
      }
    }
    //flann_matcher = cv::Ptr<FlannMatcher>(new L2FlannMatcher<float>());

    if(vocab!=""){
      load_vocabulary(vocab);
    }
  }

  vector<vector<float>> TextonBOW::compute_feature_vector(vector<Mat>& response){
    int n=response[0].rows* response[0].cols;
    vector<vector<float> > feature_vector (n,vector<float>(response.size()));
    int f=0;
    for(int i = 0; i< response[0].rows; ++i){
        for(int j = 0; j< response[0].cols; ++j){
          for(int c=0; c< response.size(); ++c){
            feature_vector[f][c]=response[c].at<float>(i,j);
          }
          f++;
        }
      }
    return feature_vector;
  }

  cv::Mat TextonBOW::compute_feature_mat(vector<Mat>& response){
    int n=response[0].rows* response[0].cols;
    int dim=response.size();
    cv::Mat descriptor(n,dim,CV_32F);
//    vector<vector<float> > feature_vector (n,vector<float>(response.size()));
    int f=0;
    for(int i = 0; i< response[0].rows; ++i){
        for(int j = 0; j< response[0].cols; ++j){
          for(int c=0; c< response.size(); ++c){
            descriptor.at<float>(f,c)=response[c].at<float>(i,j);
          }
          f++;
        }
      }
    return descriptor;
  }

  vector<unsigned int> TextonBOW::make_words(vector<Mat>& response){
   
    vector<unsigned int> out;
    return out;
  }

  vector<cv::Mat> TextonBOW::apply_filter_bank(cv::Mat& thumb){

    Mat img_c1, img_c2, img_c3;
    Mat_<float> img_c1_f, img_c2_f, img_c3_f;

    Mat_<float> img_Texton_cos(thumb.size()), img_Texton_sin(thumb.size());
    cv::Mat lab(thumb.size(), CV_8UC3);

  

    img_c1.create(thumb.size(), CV_8U);
    img_c2.create(thumb.size(), CV_8U);
    img_c3.create(thumb.size(), CV_8U);
    cv::Mat splitchannels[]={img_c1,img_c2,img_c3};
    cv::Mat_<float> splitchannels_f[]={img_c1_f,img_c2_f,img_c3_f};

    cvtColor(thumb, lab, CV_BGR2Lab);
    split(lab, splitchannels);

    //convert to floating point
    for(int i=0;i<3;++i){
      splitchannels_f[i].create(thumb.size());
      splitchannels[i].convertTo(splitchannels_f[i], CV_32F, 1.0/256.0); //img_f is between -0.5 to 0.5      
      //      imshow((string("channel:")+to_string(i)).c_str(), splitchannels_f[i]);
    }


    std::vector<cv::Mat>filter_response;

    //apply the filter bank
    //    for(double theta: thetas){
    //      for(double lambda: lambdas){
    for(int channel=0;channel<3; channel++){
      int i=0;
      for(size_t ti=0; ti< thetas.size(); ++ti){
        for(size_t li=0; li< lambdas.size(); ++li){
          double theta = thetas[ti]; double lambda = lambdas[li];

          filter2D(splitchannels_f[channel], img_Texton_cos, -1, cos_gabor[i], Point(-1,-1));
          filter2D(splitchannels_f[channel], img_Texton_sin, -1, sin_gabor[i], Point(-1,-1));
          multiply(img_Texton_cos, img_Texton_cos, img_Texton_cos);
          multiply(img_Texton_sin, img_Texton_sin, img_Texton_sin);
          cv::Mat response = img_Texton_cos + img_Texton_sin;
          float maxv = *(std::max_element(response.begin<float>(), response.end<float>()));
          response=response/maxv;
          filter_response.push_back(response);

          if(visualize){
            if(ti==0){
              cv::imshow((string("Texton  lambda:")+to_string(lambda)+" theta:"+to_string(theta)+" channel:"+to_string(channel)).c_str(), response);
            }
          }
          i++;
        }
      }
    }
    return filter_response;
  }

  WordObservation TextonBOW::operator()(cv::Mat& imgs){
    assert(imgs.data);
    Mat  thumb;
    int size_rows = size_cols*static_cast<float>(imgs.rows)/imgs.cols;
    cv::resize(imgs,thumb,cv::Size(size_cols,size_rows));

    //width of each pixel in the original image
    float word_scale = static_cast<float>(imgs.cols)/size_cols/img_scale;

    //convert the filter respone to words
    //    vector<unsigned int> words(filter_response[0].rows* filter_response[0].cols,0);
    vector<cv::Mat> filter_response = apply_filter_bank(thumb);  

    cv::Mat descriptors = compute_feature_mat(filter_response);
    //vector<vector<float>> descriptors = compute_feature_vector(filter_response);

  //  vector<unsigned int> words = make_words(filter_response);
    //copy(words.begin(), words.end(), ostream_iterator<unsigned int>(cout,",")); cout<<endl;

      
    WordObservation z;
    //flann_matcher->get_words(descriptor, z.words);
//    z.words.resize(descriptors.size());

    //vector<float> distances(z.words.size());
    cv::Mat words(descriptors.rows, 1, CV_32S);
    cv::Mat distances(descriptors.rows, 1, CV_32F);
    flann_matcher->knnSearch(descriptors,words,distances,1,cvflann::SearchParams(8));

    z.words.resize(words.rows);
    copy(words.begin<int>(), words.end<int>(), z.words.begin());
    for(int& w: z.words) w+=vocabulary_begin;

    z.source=name;
    //    z.seq = image_seq;
    //    z.observation_pose=pose;
    z.vocabulary_begin=vocabulary_begin;
    z.vocabulary_size=vocabulary_size;


    for(int i=0;i<thumb.rows; ++i) // y
      for(int j=0;j<thumb.cols; ++j){ //x
        //z.words.push_back(vocabulary_begin + words[i*thumb.cols + j]);
        //      z.words.push_back(0);
        z.word_pose.push_back(j*word_scale + word_scale/2);
        z.word_pose.push_back(i*word_scale + word_scale/2);
        z.word_scale.push_back(word_scale/2);     
      }
    //cerr<<"#color-words: "<<z->words.size();
    
    return z;
  }

  void TextonBOW::load_vocabulary(const string& filename){
    cerr<<"Reading vocbulary file: "<<filename<<endl;
    csv_reader reader(filename,','); 
    vector<vector<float> > vocab;
    while(true){
      vector<float> row = reader.get_floats();
      if(row.empty())
        break;
      vocab.push_back(row);
    }
    vocabulary.create(vocab.size(),vocab[0].size(),CV_32F);
    for(int i=0;i<vocab.size(); ++i){
      for(int j=0;j<vocab[i].size(); ++j){
        vocabulary.at<float>(i,j)=vocab[i][j];
        //cerr<<vocabulary.at<float>(i,j)<<" ";        
      }
      //cerr<<endl<<endl;
    }
    cerr<<"Vocabulary size:"<<vocabulary.rows<<"x"<<vocabulary.cols<<endl;

    //flann_matcher->set_vocabulary(vocabulary);
    flann_matcher= new cv::flann::GenericIndex<cv::flann::L2<float> > 
      (vocabulary, cvflann::KDTreeIndexParams());
    cerr<<"Returning"<<endl;
  }


