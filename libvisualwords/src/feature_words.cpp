#include <visualwords/feature_words.hpp>
#include <visualwords/flann_matcher.hpp>

FeatureBOW::FeatureBOW(int vocabulary_begin_, 
         const string& vocabulary_filename, 
         const vector<string>& feature_detector_names_, 
         const vector<int>& feature_sizes_, 
         const string& feature_descriptor_name_, 
         double img_scale_):
      BOW(feature_descriptor_name_, vocabulary_begin_),
      feature_detector_names(feature_detector_names_),
      feature_descriptor_name(feature_descriptor_name_),
      img_scale(img_scale_)
{
  cerr<<"Initializing Feature BOW with detectors:";
  for(size_t i=0;i<feature_detector_names.size(); ++i){
    cerr<<feature_detector_names[i]<<endl;
    feature_detectors.push_back(get_feature_detector(feature_detector_names[i],feature_sizes_[i]));
  }

  desc_extractor = cv::DescriptorExtractor::create(feature_descriptor_name);
  if(feature_descriptor_name=="SURF"){
    cerr<<"Using SURF descriptor"<<endl;
    desc_matcher = cv::DescriptorMatcher::create("FlannBased");
    //  desc_matcher = cv::Ptr<cv::FlannBasedMatcher>(new cv::FlannBasedMatcher());
    flann_matcher = cv::Ptr<FlannMatcher>(new L2FlannMatcher<float>());
  }
  else{
    cerr<<"Using ORB descriptor"<<endl;
    desc_matcher = cv::DescriptorMatcher::create("BruteForce-Hamming");
    flann_matcher = cv::Ptr<FlannMatcher>(new BinaryFlannMatcher());
    //desc_matcher = cv::Ptr<cv::FlannBasedMatcher>(new cv::BinaryFlannBasedMatcher());
    //desc_matcher = new cv::BinaryFlannBasedMatcher();

    //desc_matcher = cv::Ptr<cv::DescriptorMatcher>(new cv::FlannBasedMatcher(new cv::flann::LshIndexParams(12,20,2)));
    //desc_matcher = new FlannBasedMatcher(new flann::LshIndexParams(20,10,2));

  }

  //      ROS_INFO("Opening vocabulary file: %s",vocabulary_filename.c_str());
  cv::FileStorage fs(vocabulary_filename, cv::FileStorage::READ);
  if(fs.isOpened()){
    fs["vocabulary"]>>vocabulary;
    fs.release();
    cerr<<"Read vocabulary: "<<vocabulary.rows<<" "<<vocabulary.cols<<endl;
    vocabulary_size=static_cast<int>(vocabulary.rows);
  }
  else{
    cerr<<"Error opening vocabulary file"<<endl;
    exit(0);
  }
  vector<cv::Mat> vocab_vector;
  for(int i=0;i<vocabulary.rows; ++i){
    vocab_vector.push_back(vocabulary.row(i));
  }
  desc_matcher->add(vocab_vector);
  flann_matcher->set_vocabulary(vocabulary);

}
  
WordObservation FeatureBOW::operator()(cv::Mat& img){
  std::vector<cv::KeyPoint> keypoints;
  cv::Mat descriptors;

  WordObservation z;
  z.source=name;
  //      z->seq = image_seq;
  //      z->observation_pose=pose;
  z.vocabulary_begin=vocabulary_begin;
  z.vocabulary_size=vocabulary_size;

  get_keypoints(img, feature_detector_names, feature_detectors, keypoints);
  if(keypoints.size()>0) desc_extractor->compute(img,keypoints,descriptors);
  if(keypoints.size()>0) flann_matcher->get_words(descriptors, z.words);

  z.word_pose.resize(keypoints.size()*2);//x,y
  z.word_scale.resize(keypoints.size());//x,y
  auto ci = z.word_pose.begin();
  auto si = z.word_scale.begin();
  auto zi = z.words.begin();
  for(size_t ki=0;ki<keypoints.size();ki++){
    (*zi++) += vocabulary_begin; //offset the word ids by vocabulary_begin
    *ci++ = static_cast<int>(keypoints[ki].pt.x/img_scale);
    *ci++ = static_cast<int>(keypoints[ki].pt.y/img_scale);
    *si++ = static_cast<int>(keypoints[ki].size/img_scale);
  }
  //cerr<<"#feature-words: "<<z->words.size()<<endl;
  return z;
}



LabFeatureBOW::LabFeatureBOW(int vocabulary_begin_, 
                             const string& vocabulary_filename, 
                             const vector<string>& feature_detector_names_, 
                             const vector<int>& feature_sizes_, 
                             const string& feature_descriptor_name_, 
                             double img_scale_):
  BOW(string("Lab")+feature_descriptor_name_, vocabulary_begin_)
{  
  //cerr<<"Initialized LabFeatureBOW ...";;
  vocabulary_size=0;
  for(int i=0; i<3; i++){
    FeatureBOW * b = new FeatureBOW(this->vocabulary_begin, vocabulary_filename, feature_detector_names_, feature_sizes_, feature_descriptor_name_, img_scale_);
    b->vocabulary_begin=vocabulary_begin + vocabulary_size;
    //cerr<<"  vocab_begin: "<<b->vocabulary_begin<<endl;
    vocabulary_size+=b->vocabulary_size;
    bow.push_back(b);  
  }
  cerr<<"Initialized LabFeatureBOW with vocabulary of size:"<<vocabulary_size<<endl;
}


WordObservation LabFeatureBOW::operator()(cv::Mat& img){
  WordObservation z;
  z.vocabulary_size=vocabulary_size;
  z.vocabulary_begin=vocabulary_begin;
  vector<cv::Mat> img_channels;
  if(img.type() == CV_8UC1){
    img_channels.push_back(img);  
  }
  else if(img.type() == CV_8UC3 ){
    cv::Mat lab_image;
    cvtColor(img, lab_image, CV_BGR2Lab);
    split(lab_image, img_channels);
  }
  else
    assert(false);

  //cerr<<"Lab Words: ";
  for(size_t i=0;i<img_channels.size(); i++){
    WordObservation zi = (*(bow[i]))(img_channels[i]);
    z.words.insert(z.words.end(), zi.words.begin(), zi.words.end());
    z.word_pose.insert(z.word_pose.end(), zi.word_pose.begin(), zi.word_pose.end());        
    z.word_scale.insert(z.word_scale.end(), zi.word_scale.begin(), zi.word_scale.end());        
    //cerr<<i<<":"<<zi.words.size()<<endl;
    //copy(zi.words.begin(), zi.words.end(), ostream_iterator<int>(cerr,","));
    //cerr<<endl;
  }
  //cerr<<endl;
  return z;
}
