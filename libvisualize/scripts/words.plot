#!/usr/bin/env python
import csv
import os
import subprocess
from optparse import OptionParser
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib
import numpy as np

parser = OptionParser()
parser.add_option("-i", "--in.topics", dest="wfilename", 
                  help="input word FILE", metavar="FILE")
parser.add_option("-p", "--in.positions", dest="pfilename",
                  help="input word position FILE", metavar="FILE")
parser.add_option("-o", "--out.topics", dest="oimgfilename", default="topics.png",
                  help="output image filename FILE", metavar="FILE")
parser.add_option("-x", "--width", dest="imgwidth", default=0,
                  help="Width od the image. if 0, then automatic", metavar="WIDTH")
parser.add_option("-y", "--height", dest="imgheight", default=0,
                  help="Height of the image. if 0, then automatic", metavar="HEIGHT")
parser.add_option("-s", "--scale", dest="scale", default=1,
                  help="scale = 2 implies shrink the output image by half", metavar="SCALE")
parser.add_option("--in.path", dest="pathfilename", default="",
                  help="input path FILE", metavar="FILE")
parser.add_option("--out.path", dest="opathfilename", default="path.png",
                  help="input path FILE", metavar="FILE")

(options, args) = parser.parse_args()

def read_timestamped_csv(filename):
	rows=[]
	K=0
	with open(filename, 'rb') as f:
	    reader = csv.reader(f)
	    for row in reader:
	    	if row[0][0] == '#':
	    		None
	        else:
	        	rows.append(map(int,row))
	return rows

flattened_wdata=[]
flattened_pdata=[]
draw_words=False
if options.wfilename != "":
	draw_words=True
	wdata=[]
	pdata=[]
	print('Reading word file')
	wdata = np.array(read_timestamped_csv(options.wfilename))
	flattened_wdata=[]
	for row in wdata:
		flattened_wdata.extend(row[1:])
	print('Reading position file')
	pdata = np.array(read_timestamped_csv(options.pfilename))
	flattened_pdata=[]
	for row in pdata:
		flattened_pdata.extend(row[1:])


path = []
draw_path = False
if options.pathfilename != "":
	path = read_timestamped_csv(options.pathfilename)
	draw_path=True

print('Done')

def plot_words(words,positions,oname,imgw,imgh,scale):
	x=positions[0::2]
	y=positions[1::2]
	x_min=np.min(x)
	x_max=np.max(x)
	y_min=np.min(y)
	y_max=np.max(y)
	dx=x_max-x_min+1
	dy=y_max-y_min+1
	if imgw!=0:
		dx=imgw;
		x_min=0
		x_max=imgw-1
	if imgh!=0:
		dy=imgh;
		y_min=0
		y_max=imgh-1

	weights = np.bincount(words)
	ordering = np.argsort(weights)[::-1]
	print "Topic ordering:", ordering
	print "Topic weights:", weights[ordering]
	print x_min, x_max, y_min, y_max, dy, dx, len(x), len(y), len(words)
	img=np.zeros((dy,dx),dtype=np.int32)
	img -= 1
	arr = np.arange(len(words))
 	np.random.shuffle(arr)
	for i in arr:
		px=x[i]-x_min
		py=y[i]-y_min
		w=(ordering[words[i]] % 256)
		img[py/scale,px/scale]=w

	fig = plt.figure(frameon=False)
	fig.set_size_inches(1,float(dy)/float(dx))
	ax = fig.add_axes([0, 0, 1, 1])
	#ax.axis('off')
	ax.set_axis_off()
	cmap = matplotlib.cm.jet
	cmap.set_bad('w',0.)
	masked_array = np.ma.masked_where(img==-1, img)
	p=ax.imshow(masked_array,interpolation='nearest', cmap=cmap)
	#p.set_interpolation('nearest')
	#plt.axis('off')
	#if oname!="":
	print "Writing ",oname
	extent = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
	fig.savefig(oname,bbox_inches=extent,dpi = dx,pad_inches=0)
	#else:
	#	plt.show()

def plot_path(fig,filename,steps,imgw,imgh,scale):
	x = steps[:,1]/scale
	y = steps[:,2]/scale
	t = steps[:,0]	
	print x,y
	imgw = max(max(x)+1,imgw)
	imgh = max(max(y)+1,imgh)
	ax=fig.add_axes([0,0,1,1]) # [left,bottom,width,height] in 0-1 coordinate system
	colors= matplotlib.cm.jet(np.linspace(0, 1, len(x)))
	ax.scatter(x,imgh-y,c=t,s=1,marker='.',lw=0)
	ax.set_axis_off()
	ax.set_xlim(0, imgw)
	ax.set_ylim(0, imgh)
	fig.set_size_inches(1,float(imgh)/float(imgw))
	print "Writing ",filename
	fig.savefig(filename, dpi=imgw, pad_inches=0,transparent=True)
	#plt.show()

fig = plt.figure(1)
if draw_path:
	plot_path(fig,options.opathfilename, np.array(path), int(options.imgwidth), int(options.imgheight), int(options.scale))

if draw_words:
	plot_words(flattened_wdata,flattened_pdata,options.oimgfilename, int(options.imgwidth), int(options.imgheight), int(options.scale))
