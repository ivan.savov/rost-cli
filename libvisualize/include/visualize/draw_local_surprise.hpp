#ifndef ROST_DRAW_LOCAL_SURPRISE
#define ROST_DRAW_LOCAL_SURPRISE
#include <algorithm>
#include <numeric>

using namespace std;

template<typename T>
cv::Mat draw_pose_surprise(const T& pose_surprise, cv::Mat img){

  float max_surprise = pose_surprise.begin()->second;
  float min_surprise = pose_surprise.begin()->second;

  for(auto ps: pose_surprise){
    max_surprise = max(max_surprise, ps.second);
    min_surprise = min(min_surprise, ps.second);
  }

  for(auto ps: pose_surprise)
    auto pose=ps.first;
    img_surp.at<float>(ps[1],ps[2])=ps.second / max_surprise;
  }
  return img;
}

#endif

