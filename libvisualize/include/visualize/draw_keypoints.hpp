#ifndef ROST_VISUALIZE_DRAW_KEYPOINTS
#define ROST_VISUALIZE_DRAW_KEYPOINTS
#include <algorithm>
using namespace std;
cv::Mat get_colors(int n_colors=32, int value=255){
  cv::Mat bgr_colors, hsv_colors;
  hsv_colors.create(n_colors,1,CV_8UC3);
  bgr_colors.create(n_colors,1,CV_8UC3);
  for(int i=0;i<n_colors; ++i){
    hsv_colors.at<cv::Vec3b>(i,0) = cv::Vec3b(160/n_colors*i,255,value);
  }
  cv::cvtColor(hsv_colors, bgr_colors, CV_HSV2BGR);
  return bgr_colors;
}
cv::Scalar get_color(int i){
  static cv::Mat bgr_colors_high = get_colors(16,255);
  static cv::Mat bgr_colors_low = get_colors(16,127);
  i = i%32;
  if(i<16){
    return cv::Scalar (bgr_colors_high.at<cv::Vec3b>(i,0)[0],
		       bgr_colors_high.at<cv::Vec3b>(i,0)[1],
		       bgr_colors_high.at<cv::Vec3b>(i,0)[2]);
  }
  else{
    return cv::Scalar (bgr_colors_low.at<cv::Vec3b>(i-16,0)[0],
		       bgr_colors_low.at<cv::Vec3b>(i-16,0)[1],
		       bgr_colors_low.at<cv::Vec3b>(i-16,0)[2]);
  }
}

cv::Mat draw_keypoints(const WordObservation& z, cv::Mat img, float scale=0){
  //static std::random_device rd;
  //static std::mt19937 g(rd());

  int n_colors = min<int>(16, z.vocabulary_size);
  cv::Mat bgr_colors = get_colors(n_colors);
  vector<vector<cv::KeyPoint> > keypoints(n_colors);
  cv::Mat out_img = img.clone();
  if(z.word_pose.size() == 2* z.words.size()){
    //    vector<cv::KeyPoint> keypoints(z.words.size());
    vector<size_t> shuffled_index(z.words.size());
    for(size_t i=0; i< z.words.size(); ++i){
      shuffled_index[i]=i;
    }    
    random_shuffle(shuffled_index.begin(), shuffled_index.end());
    for(size_t j=0; j< z.words.size(); ++j){
      size_t i = shuffled_index[j];
      size_t color = (z.words[i] - z.vocabulary_begin)%n_colors;
      float word_scale= (scale== 0)? z.word_scale[i]:scale;
      keypoints[color].push_back(cv::KeyPoint(static_cast<float>(z.word_pose[i*2]),
      static_cast<float>(z.word_pose[i*2+1]),
      static_cast<float>(word_scale)));             
    }

    for(size_t i=0;i<keypoints.size(); ++i){
      cv::drawKeypoints(out_img, keypoints[i], out_img, get_color(i), cv::DrawMatchesFlags::DRAW_OVER_OUTIMG + cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
    }
  }
  return out_img;

}

cv::Mat colorize(const cv::Mat& f, const cv::Vec3b& color, const cv::Vec3b& maxcolor){
  cv::Mat c(f.rows, f.cols, CV_8UC3);
  auto max_it = max_element(f.begin<float>(), f.end<float>());
  auto it_c = c.begin<cv::Vec3b>();
  for(auto it_f = f.begin<float>(); it_f != f.end<float>() ; ++it_f, ++it_c){
    if(it_f == max_it)
      *it_c = maxcolor;
    else for(int i=0; i<3; i++)
      (*it_c)[i] = cv::saturate_cast<uchar>(color[i] * (*it_f));
  }
  return c;
}
#endif
